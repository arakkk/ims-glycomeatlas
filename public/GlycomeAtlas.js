/*------------------------------ここからJavaScript------------------------------*/

/*------------------------------グローバル変数の宣言------------------------------*/
var Click_Glycan_Store = []; //糖鎖詳細画面に表示しているLinearCodeを保存
var PV_Click_Glycan_Store = []; //Profile Viewer用の糖鎖詳細画面に表示しているLinearCodeを保存

var Glycan_Draw_Flag = false; //右側のペインに糖鎖構造が描画されているかどうか
var Open_Profile_Viewer = false; //Profile Viewerを開いているかどうか

//IDname=resultのdocumentオブジェクトをresult変数に
var r = document.getElementById("result");

//初期状態のProfile Viewerの全タグとその内容を保存。Profile Viewerを閉じたときに、今までの変更を全て破棄し、デフォルトの状態で復元する
var default_profile_viewer = document.getElementById("Default_Profile_Viewer").innerHTML;

//Linear CodeをGlycanImageで表示するときに、必要ない情報を削除するために必要な正規表現
//ただし、Linear Codeをコピーする際などには、必要な情報が欠落しないようにそのまま残してある（あくまで表示上のみ削除している）
//現在、
//①#Cerなどの結合情報（#以降の文字列全て）
//を消去している。
var filter_regexp = /(#\w)/g;

/*------------------------------グローバル変数の宣言ここまで------------------------------*/

/*------------------------------get URL parameters------------------------------*/
function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
    vars[key] = decodeURI(value);
  });

  return vars;
}
/*------------------------------getUrlVarsここまで------------------------------*/

/*------------------------------Profile Viewerが開かれているかどうかを判断する関数------------------------------*/
function add_clone_(strings) {
  //Profile_Viwerが開かれていたら、引数に_cloneを付加して返す。そうでなければ（メイン画面なら）何も付加せずに返す
  //様々なHTML要素を取得するときに使用する
  return (strings += Open_Profile_Viewer ? "_clone" : "");
}
/*------------------------------add_clone_関数ここまで------------------------------*/

/*------------------------------初期設定（起動時の処理）------------------------------*/
/*ここの処理で、TissueListの読み込み、LCListの読み込み、SVGファイルの読み込みの計3種類の処理を行っている*/
/*ここでのループは生物種の数や組織の数によって条件が変動するようになっているため、今後増えたとしても書き換える必要はない*/
//TissueListの読み込みと、sessionStorageへの保存
$.get("./TissueList.json", function (data) {
  sessionStorage.setItem("TissueList", JSON.stringify(data)); //TissueListを保存

  LCstring = getUrlVars()["LC"];
  selectedGTCID = getUrlVars()["GTC"];
  selectSpecies = getUrlVars()["species"]; //"Zebrafish";
  selectTissue = getUrlVars()["tissue"]; //"Gill";
  console.log(selectedGTCID);

  //LCListの読み込みと、sessionStorageへの保存
  var TissueList = Get_TissueList(); //TissueListを取得

  //TissueListに記載されている生物種の数だけループ
  for (var i = 0; i < TissueList["result"]["species"].length; i++) {
    //TissueListからボタンタグを作成
    var button = "<button id=";
    button += TissueList["result"]["species"][i]["$"]["name"]; //IDに生物種名を追加
    button += " name=";
    button += TissueList["result"]["species"][i]["$"]["name"]; //nameに生物種名を追加
    //button += ' onclick="species_branch(this.getAttribute("name"),true)"';   //onclickイベントを追加
    button += ' onclick="species_branch(this,true)"'; //onclickイベントを追加

    //もし、i=0（Human）のときだったら、既に押された状態を作る
    //if( i==0 )
    if (TissueList["result"]["species"][i]["$"]["name"] != selectSpecies) {
      button += 'disabled="disabled"';
    }

    button += ">";
    button += TissueList["result"]["species"][i]["$"]["name"]; //実際にボタンに表示される生物種名を追加
    button += "</button>";

    //ボタンを配置
    $("#button_group").append(button);

    /*ヒトとマウスの組織の画像であるSVGが記載されているhtmlファイルを読み込む＋SessionStorageに保存*/
    //ただし、Chromeのみターミナルから「open -a Google\ Chrome --args --allow-file-access-from-files」コマンドを打たないといけない
    //Chromeは、ローカルファイルにもクロスオリジンを発生させてるっぽい
    (function (i) {
      var filename = TissueList["result"]["species"][i]["$"]["name"] + "_SVG";
      var filepath = filename + ".html";

      $.get(filepath, function (data) {
        sessionStorage.setItem(filename, data);
      });
    })(i);

    //TissueListに記載されている生物種の組織の数だけループ
    for (var j = 0; j < TissueList["result"]["species"][i]["tissue"].length; j++) {
      //LCListを1つずつ取得・保存する
      (function (i, j) {
        //TissueListから、取得するLCListのファイルネームを作成（「生物種名-組織名」がファイル名）
        var filename = TissueList;
        filename =
          TissueList["result"]["species"][i]["$"]["name"] +
          "-" +
          TissueList["result"]["species"][i]["tissue"][j]["$"]["name"];
        //作成したファイル名を持つファイルは上階層のディレクトリにあるため、ディレクトリ名「LCList」と、拡張子「.json」を付加
        var filepath = "LCList/" + filename + ".json";

        // find LC for GTCID
        if (
          selectedGTCID != undefined &&
          TissueList["result"]["species"][i]["$"]["name"] == selectSpecies &&
          TissueList["result"]["species"][i]["tissue"][j]["$"]["name"] == selectTissue
        ) {
          LCstring = getLCFromGTC(selectSpecies, selectTissue, selectedGTCID);
        }

        //非同期通信を行って、LCListを保存する。
        $.get(filepath, function (data) {
          sessionStorage.setItem(filename, JSON.stringify(data));
          //document.getElementById("main").innerHTML = sessionStorage.getItem("Human_SVG");
          //document.getElementById("main").innerHTML = sessionStorage.getItem(TissueList["result"]["species"][i]["$"]["name"]+"_SVG");
        });
      })(i, j);
    }
  }

  Create_TissueList_Table();

  SelectLC(LCstring);

  console.log("Done");
});
/*------------------------------初期設定（起動時の処理）ここまで）------------------------------*/

function SelectLC(selectedLC) {
  document.getElementById(add_clone_("list_table")).childNodes[3].setAttribute("data-select", 1);
  /* if URL has vars, select glycan using LC */
  selectedGTCID = getUrlVars()["GTC"];
  selectSpecies = getUrlVars()["species"]; //"Zebrafish";
  selectTissue = getUrlVars()["tissue"]; //"Gill";
  tissue = selectTissue;
  //selectTissue = selectSpecies + "-" + selectTissue;

  // check if using GTC ID
  if (selectedGTCID != undefined) {
  }
  if (
    selectedLC == undefined ||
    selectSpecies == undefined ||
    selectTissue == undefined ||
    selectSpecies == null ||
    selectedLC == null ||
    selectTissue == null
  ) {
    selectSpecies = "Human";
    species_branch(selectSpecies);
    return;
  }
  species_branch(selectSpecies, true);

  Create_LCList_Table(selectSpecies, tissue, "", 2);

  var i = reg_check(selectedLC);

  if (i) {
    //選択が解除された糖鎖構造の糖鎖詳細画面上の表を削除
    //表の大枠となっているIDを取得
    var detailed_table_id = document.getElementById(add_clone_("Detailed_table"));

    if (!Open_Profile_Viewer) {
      //選択中の全糖鎖数から選択した糖鎖の配列格納番号を引く（表が逆順に出力されているため）
      //そこからさらに1を引く（lengthが最大要素番号ではなく＋1の要素数を出力するため）
      detailed_table_id.deleteRow(Click_Glycan_Store.length - i);

      //配列から特定の糖鎖構造だけを削除
      Click_Glycan_Store.splice(i - 1, 1);
    } else {
      //選択中の全糖鎖数から選択した糖鎖の配列格納番号を引く（表が逆順に出力されているため）
      //そこからさらに1を引く（lengthが最大要素番号ではなく＋1の要素数を出力するため）
      detailed_table_id.deleteRow(PV_Click_Glycan_Store.length - i);

      //配列から特定の糖鎖構造だけを削除
      PV_Click_Glycan_Store.splice(i - 1, 1);
    }
  } else {
    //Detailed_tableに糖鎖構造を大きく表示させる
    var strings = '<tr oncontextmenu="return false;" class="Detailed_class';
    strings += Open_Profile_Viewer ? '_clone" ' : '" ';
    strings += 'data-select="';
    strings += disabled_button_search("strings") + " " + selectTissue; //現在選択している生物種と組織を独自データ属性(data-select)に追加
    strings += '">';
    strings += '<td class="Detailed_td';
    strings += Open_Profile_Viewer ? '_clone" ' : '" ';
    strings += 'id="Detailed_td';
    strings += Open_Profile_Viewer ? PV_Click_Glycan_Store.length : Click_Glycan_Store.length;
    strings += '" name="';
    strings += selectedLC;
    strings += '">';
    strings += GlycanImage.imageLC(selectedLC.replace(filter_regexp, ""), "svg", 1.7);
    strings += '<span class="detailed_cross';
    strings += Open_Profile_Viewer ? '_clone">' : '">';
    strings += "✕</span></td></tr>";

    //配列にLinearCodeをプッシュ（左：Profile Viewerに使用する配列に格納　右：メインに使用する配列に格納）
    Open_Profile_Viewer
      ? PV_Click_Glycan_Store.push(split_and_marge(selectedLC, false, false))
      : Click_Glycan_Store.push(split_and_marge(selectedLC, false, false));

    $(add_clone_("#Detailed_table")).prepend(strings);
  }

  //背景色が変わった糖鎖が存在する組織と表を全て色付けする
  query_function();
  var viewing = document.getElementById(add_clone_("viewing"));
  //もしsubtissue_nameが空でなければ、通常の「生物種名-組織名」に加えて、「-副組織名」を追加する
  viewing.innerHTML = disabled_button_search("strings") + "-" + selectTissue;
}

/*------------------------------生物種のIDを取得し、SessionStorageに保存してあるTissueListから表を作成する関数------------------------------*/
function Create_TissueList_Table() {
  var make_table = "";
  var list = Get_TissueList(); //TissueListを取得

  var disabled_species = disabled_button_search("number"); //現在選択中の生物種の番号を取得

  var loop = list["result"]["species"][disabled_species]["tissue"].length; //ループ回数を代入

  var append_table_id = add_clone_("#list_table"); //TissueListを追加する表のIDを取得（Profile Viewerなのかメインなのかを判断）

  var k = 0; //kはProfile Viewerの表のtrID「list_tr_clone」の番号として使用

  //TissueListの表を書き換えるため、一旦表示されている表を全て削除する
  var tissue_table = document.getElementById(add_clone_("list_table"));
  while (tissue_table.rows.length > 1) tissue_table.deleteRow(-1);

  //表の作成
  //組織の数だけループ
  for (var i = 0; i < loop; i++) {
    //Profile Viewerでないときは単一ループ
    if (!Open_Profile_Viewer) {
      make_table += "<tr class='list_tr' id='list_tr" + i + "' align='center' style='font-size: 9px'>";
      make_table += "<td>";
      make_table += list["result"]["species"][disabled_species]["tissue"][i]["$"]["id"]; //TissueID
      make_table += "</td>";
      make_table += "<td>";
      make_table += list["result"]["species"][disabled_species]["tissue"][i]["$"]["name"]; //TissueName
      make_table += "</td>";
      make_table += "<td>";
      make_table += list["result"]["species"][disabled_species]["tissue"][i]["$"]["cnt"]; //Tissueに局在する糖鎖数
      make_table += "</td>";
      make_table += "</tr>";
    }

    //Profile Viewerの場合は二重ループ
    else {
      //先にTissue Nameを追加
      make_table += "<tr class='list_tr' id='list_tr_clone" + k + "' align='center' style='font-size: 9px'><td>";
      make_table += list["result"]["species"][disabled_species]["tissue"][i]["$"]["tissue_name"]; //Tissue_Name

      //単一ループではTissueの数でループさせていたが、このループはTissueに存在するSubtissueの数分ループする（最低1回はループ）
      for (var j = 0; j < list["result"]["species"][disabled_species]["tissue"][i]["subtissue"].length; j++) {
        //jが0でない場合＝subtissueが複数存在した場合、Tissue Nameを空白にする
        if (j != 0) {
          make_table += "<tr class='list_tr' id='list_tr_clone" + k + "' align='center' style='font-size: 9px'><td>";
          make_table += ""; //Tissue_Name
        }

        make_table += "</td><td>";
        make_table += list["result"]["species"][disabled_species]["tissue"][i]["subtissue"][j]["$"]["subtissue_name"]; //SubTissue_Name
        make_table += "</td><td>";
        make_table += list["result"]["species"][disabled_species]["tissue"][i]["subtissue"][j]["$"]["cnt"]; //SubTissueに局在する糖鎖数
        make_table += "</td></tr>";

        k++;
      }
    }
  }

  $(append_table_id).append(make_table); //できあがった表を、指定したIDのテーブルに表示
}

/*------------------------------Create_TissueList_Table関数ここまで------------------------------*/

/*------------------------------生物種のIDと組織のIDを引数としてSessionStorageに保存してあるLCListから糖鎖構造を描画する関数------------------------------*/
function Create_LCList_Table(species_name, tissue_name, subtissue_name, tissue_ID) {
  /*LCListとクリックされた行番号をもとに、Glycanの表を作成*/
  var table = document.getElementById(add_clone_("Glycan_table"));
  while (table.rows.length > 0) table.deleteRow(-1);

  var list = Get_LCList(species_name, tissue_name, subtissue_name);
  var make_table = "";
  var data_tmp = Get_TissueList();
  var data;
  var i = 0;

  if (!Open_Profile_Viewer) {
    r.innerHTML = "";

    make_table += "<tr id='Glycan_tr'>";

    // tissue count
    data = data_tmp["result"]["species"][disabled_button_search("number")]["tissue"][tissue_ID]["$"]["cnt"];
    if (list["result"]["item"] == undefined) {
      data = 0;
    } else {
      data = list["result"]["item"].length;
    }

    for (; i < data; i++) {
      //一時的なエラー回避用if文（Linear Codeがunknownだったら飛ばす）
      if (list["result"] == "" || list["result"]["item"][i]["code"][0] === "unknown") {
        console.log("LC not found in this tissue of this species");
        continue;
      } else {
        make_table += "<td class='Glycan_class' id='Glycan_td";
        make_table += i;
        make_table += "'";
        make_table +=
          " width='200px' height='auto' style='border-style: none; margin-bottom: 10px; display: inline-block;'>";
        make_table += GlycanImage.imageLC(list["result"]["item"][i]["code"][0].replace(filter_regexp, ""), "svg", 1.7);
      }

      make_table += "</td>";
    }

    make_table += "</tr>";
  } else {
    make_table += "<tr id='Glycan_tr_clone'>";

    data_tmp = data_tmp["result"]["species"][disabled_button_search("number")]["tissue"][tissue_ID]["subtissue"];

    for (var j = 0; j < data_tmp.length; j++) {
      if (data_tmp[j]["$"]["subtissue_name"] === subtissue_name) {
        data = data_tmp[j]["$"]["cnt"];
        break;
      }
    }

    for (; i < data; i++) {
      make_table += "<td class='Glycan_class' id='Glycan_td_clone";
      make_table += i;
      make_table += "'";
      make_table +=
        " width='200px' height='auto' style='border-style: none; margin-bottom: 10px; display: inline-block;'>";
      make_table += GlycanImage.imageLC(list["result"]["item"][i]["code"][0].replace(filter_regexp, ""), "svg", 1.5);
      make_table += "</td>";
    }

    make_table += "</tr>";
  }

  //糖鎖描画画面に要素（糖鎖構造）を追加
  $(add_clone_("#Glycan_table")).append(make_table);

  //もしProfile Viewerが開かれていたら、右下のグラフ描画画面にグラフを描画する
  //（糖鎖構造を糖鎖描画画面にappendする前に実行すると、グラフエリアのwidth値がデフォルトの200pxのままであるので、ここで関数を実行する）
  if (Open_Profile_Viewer) {
    //現在表示しているグラフ描画画面があるtdタグの幅の95%のサイズを取得（100%だとややはみ出るため）
    //そして、グラフ描画画面に初期値として設定してある200pxから取得したサイズに変更する
    var max_table_width = document.getElementById("graph").getBoundingClientRect().width * 0.95;
    document.getElementById("graph_table").width = max_table_width;

    //Profile Viewerのときは、グラフ画面にグラフを作成
    draw_graph(list);
  }

  //もし糖鎖構造が0だったとき、糖鎖描画画面に赤太文字で「No Data」と表示させる
  if (i == 0) {
    r.innerHTML = "No Data".big();
    r.style.color = "red";
  }

  query_function();

  Glycan_Draw_Flag = true; //一度実行されたら、以後、ずっとtrue
}
/*------------------------------Create_LCList_Table関数ここまで------------------------------*/

/*------------------------------JSONをパースしたTissueListを返す関数------------------------------*/
function Get_TissueList() {
  //ProfileViewerが開かれているならば、取得するファイル名の最初にUserProfile_を付ける
  var UserProfile = Open_Profile_Viewer ? "UserProfile_" : "";
  return JSON.parse(sessionStorage.getItem(UserProfile + "TissueList"));
}
/*------------------------------Get_TissueList関数ここまで------------------------------*/

/*------------------------------JSONをパースしたLCListを返す関数------------------------------*/
function Get_LCList(species_name, tissue_name, subtissue_name) {
  //ProfileViewerが開かれているならば、取得するファイル名の最初にUserProfile_を付ける
  var UserProfile = Open_Profile_Viewer ? "UserProfile_" : "";

  if (subtissue_name === "") {
    return JSON.parse(sessionStorage.getItem(UserProfile + species_name + "-" + tissue_name));
  } else {
    return JSON.parse(sessionStorage.getItem(UserProfile + species_name + "-" + tissue_name + "-" + subtissue_name));
  }
}
/*------------------------------Get_LCList関数ここまで------------------------------*/

/*------------------------------生物種によってTissueListを変更する関数------------------------------*/
function species_branch(button) {
  //押されたボタンの生物種名を取得
  var species_name = typeof button == "string" ? button : button.getAttribute("name");

  //メインタグを取得（Profile Viewerのパターンと、通常のパターンをprofileのtrue/falseで判断
  var main_tag = Open_Profile_Viewer ? document.getElementById("Profile_Viewer") : document.getElementById("main");

  //ボタン（生物種）の子要素全てを取得。
  var all_species_button = document.getElementById(add_clone_("button_group")).children;

  //子要素の数だけループ（通常であれば0〜２の3回、Profile_Viewerを使用していればそのプロファイルの生物種分）
  for (var i = 0; i < all_species_button.length; i++) {
    //もし押したボタンと同じ生物種になったら、ボタンを無効（disabled）にする。それ以外は全て有効にする
    all_species_button[i].disabled = all_species_button[i].innerHTML === species_name ? true : false;
  }

  //組織表のtbodyに設定した現在選択中の組織IDを削除する
  document.getElementById(add_clone_("list_table")).childNodes[3].removeAttribute("data-select");
  //No Resultを糖鎖詳細画面に表示させたままだと、他の生物種に切り替えたときに残ってしまうため、ここで削除（空文字にする）
  document.getElementById("result").innerHTML = "";

  //Profile_Viewerを使用していないときの処理
  if (!Open_Profile_Viewer) {
    //押されたボタンの生物種名から、sessionStorageより画像を持ってきて表示
    main_tag.innerHTML = sessionStorage.getItem(species_name + "_SVG");

    //左上の方にある生物種名を変更する
    document.getElementById("species").innerHTML = species_name;
  }

  //さらに、糖鎖構造画面（右ペイン）に描かれている糖鎖表を全て削除
  var glycan_table = document.getElementById(add_clone_("Glycan_table"));
  while (glycan_table.rows.length > 0) glycan_table.deleteRow(-1);

  //糖鎖詳細画面に表示してある現在選択している「生物種名と組織名」を「Nothing-」に戻す
  var viewing = document.getElementById(add_clone_("viewing"));
  viewing.innerHTML = "Nothing-";

  Create_TissueList_Table(); //TissueListの表をここで書き換える

  query_function(); //全組織で横断検索（色付け）
}
/*------------------------------species_branch関数ここまで------------------------------*/

/*------------------------------表をクリックすることで組織別によって取得するLCListを変更するイベント------------------------------*/
$(document).on("click", ".list_tr, .list_tr_clone", function () {
  var tableID = $(this).closest("tr").index(); //表のID(Number:0-15)を取得

  //ここでTissueNameを取得するための配列番号をProfileViewerかそうでないかで分ける
  //通常であれば、0→TissueID, 1→TissueName, 2→Dataなので、1が、
  //ProfileViewerであれば、0→TissueName, 1→SubTissueName, 2→Glycan(Data)なので0が代入される
  var get_Tissue_name_index = Open_Profile_Viewer ? 0 : 1;
  var tissue_name = this.childNodes[get_Tissue_name_index].innerHTML; //クリックした表のTissueNameを取得
  var subtissue_name = Open_Profile_Viewer ? this.childNodes[get_Tissue_name_index + 1].innerHTML : ""; //SubTissueNameも（Profile Viewerのときだけ）取得

  //組織表に属性data-selectを追加し、IDを付加する（現在選択している組織のID）
  document.getElementById(add_clone_("list_table")).childNodes[3].setAttribute("data-select", tableID);

  //Profile_Viwerだったら、クリックしたセル中のTissueNameが空白の場合があるので、空セルの数だけクリックした行番号から引き算し、tableID=tissueIDをそろえる
  //クリックした現在の位置から上へと参照していき、選択したセルが空セル（TissueNameが存在しなかった時）の場合、最初に登場した組織が空セルの組織名となる
  //このループは一番上まで必ず実行しなければならない。上の方で代入されたtableIDは空セルを含めた行番号であり、Tissueの数と一致しないためである
  //そのため、ここでいくつ空白のセルがあったかを数え、選択したセルの行番号から空セルの分を引くことで、選択したTissueNameが上から何番目のものかを判断する
  if (Open_Profile_Viewer) {
    var empty_cells = 0;
    var flag = true;

    //表の行数をクリックしたセルの位置から上に向かってループする
    for (var i = tableID; i >= 0; i--) {
      //もし、TissueNameが空白だったら、空セルとして変数に1プラスする
      if ($("#list_tr_clone" + i).closest("tr")[0].childNodes[0].innerHTML === "") {
        empty_cells++;
      }

      //TissueNameが空白じゃなかった場合、実行される
      else if (flag) {
        //空白セルじゃない場合、tableIDと同じ組織名をここで代入し、選択したセルにTissueNameがあろうがなかろうがこの組織が選択されたとする
        tissue_name = $("#list_tr_clone" + i).closest("tr")[0].childNodes[0].innerHTML;
        flag = false;
      }
    }

    tableID -= empty_cells;
  }

  Create_LCList_Table(disabled_button_search("strings"), tissue_name, subtissue_name, tableID); //LCListを書き換える（最初の引数は生物種の番号が、最後の引数はクリックした表の番号（を空セルから引いたもの）が入る）

  //糖鎖表示画面の上部にどの生物種のどの組織を選択したかを表示
  var viewing = document.getElementById(add_clone_("viewing"));
  //もしsubtissue_nameが空でなければ、通常の「生物種名-組織名」に加えて、「-副組織名」を追加する
  viewing.innerHTML =
    subtissue_name === ""
      ? disabled_button_search("strings") + "-" + tissue_name
      : disabled_button_search("strings") + "-" + tissue_name + "-" + subtissue_name;
});
/*------------------------------LCListを書き換えるon("click")イベントここまで------------------------------*/

/*------------------------------SVGのクリックでクリックした組織画像の色を固定、LCListを取得し、糖鎖構造一覧を書き換える関数------------------------------*/
function click(obj) {
  //クリックしたSVG画像のIDを取得
  var svgID = Number(obj.id);

  //SVGのIDから選択した組織と同じ組織の行を選び出し、TissueNameを取得する
  var tissue_name = document.getElementById("list_tr" + svgID).childNodes[1].innerHTML;

  //組織表のtbodyに属性data-selectを追加し、IDを付加する（現在選択している組織のID）
  document.getElementById("list_table").childNodes[3].setAttribute("data-select", svgID);

  //現在選択している生物種名を取得
  var select_species = disabled_button_search("strings");

  //LCListの書き換え（ProfileViewerが開かれている場合、そもそもSVG画像は表示されないのでSubTissueNameは空文字となっており、別の処理を記述する必要はない
  Create_LCList_Table(select_species, tissue_name, "", svgID);

  //糖鎖表示画面の上部にどの生物種のどの組織を選択したかを表示
  var viewing = document.getElementById("viewing");

  //糖鎖描画画面の上部に、選択している生物種と組織の名前を表示
  viewing.innerHTML = select_species + "-" + tissue_name;
}
/*------------------------------click関数ここまで------------------------------*/

/*------------------------------糖鎖描画画面に描画された糖鎖の背景色を変更する関数------------------------------*/
function change_Glycan_background_color(s_name, t_name, st_name, data) {
  //右側のペインに糖鎖構造が描かれていなければ、この先でのエラー回避のためreturn
  if (!Glycan_Draw_Flag) {
    return false;
  }

  //現在表示しているTissueの糖鎖構造の内、既に別のTissueで選択された糖鎖を色付け
  //そのTissueが持つ全ての糖鎖構造をループで検索
  for (var i = 0; i < data; i++) {
    var LC = Get_LCList(s_name, t_name, st_name);
    LC = LC["result"]["item"][i]["code"][0];

    //serumに含まれるLinearCodeの内、unknownは飛ばす
    if (LC === "unknown") continue;

    var click_glycan = document.getElementById(add_clone_("Glycan_td") + i);
    var graph = Open_Profile_Viewer ? document.getElementById("graph_" + i) : "";
    var search = Open_Profile_Viewer ? PV_Click_Glycan_Store.join("|") : Click_Glycan_Store.join("|");

    //もし、その組織に既に選択している糖鎖構造が見つかれば、糖鎖構造の背景色を桃色に、見つからなければ白色に変更する
    click_glycan.style.backgroundColor = search !== "" && RegExp(search).test(LC) ? "rgba(255,0,255,0.3)" : "";

    //Profile Viewerが開いていたとき
    if (Open_Profile_Viewer) {
      //もし、その組織に既に選択している糖鎖構造が見つかれば、グラフの塗りつぶし色を桃色に、見つからなければ緑色に変更する
      graph.style.fill = search !== "" && RegExp(search).test(LC) ? "rgba(255,0,255,0.3)" : "";
    }
  }
}
/*------------------------------change_Glycan_background_color関数ここまで------------------------------*/

/*------------------------------組織画像、組織表、グラフなどの色を変更する関数------------------------------*/
function change_color(svg, table, color) {
  //もし、svgがundefinedだったら、エラーの原因になるのでこの後の処理を無視する
  if (svg === undefined || svg == null) {
  } else {
    if (!Open_Profile_Viewer) {
      switch (color) {
        case "pink":
          {
            svg.style.opacity = 0.7;
            svg.style.fill = "rgb(200,0,200)";
            svg.style.stroke = "rgb(130,0,130)";
            table.style.backgroundColor = "rgb(250,170,250)";
          }
          break;

        case "yellow":
          {
            svg.style.opacity = 0.7;
            svg.style.fill = "rgb(255,215,0)";
            svg.style.stroke = "rgb(200,115,0)";
            table.style.backgroundColor = "rgb(255,215,0)";
          }
          break;

        default:
          //"white":
          {
            svg.style.opacity = 0.6;
            svg.style.fill = "";
            svg.style.stroke = "";
            table.style.backgroundColor = "";
            //値を指定しない＝最初の状態に戻す
          }
          break;
      }
    }

    //Profile Vieweの場合
    else {
      switch (color) {
        case "pink":
          {
            table.style.backgroundColor = "rgb(250,170,250)";
          }
          break;

        case "yellow":
          {
            table.style.backgroundColor = "rgb(255,215,0)";
          }
          break;

        default:
          //"white":
          {
            table.style.backgroundColor = "";
            //値を指定しない＝最初の状態に戻す
          }
          break;
      }
    }
  }
}
/*------------------------------change_color関数ここまで------------------------------*/

/*------------------------------糖鎖構造を検索する際、正規表現によって存在するかどうかを判断する関数------------------------------*/
function reg_check(LC) {
  var CGS = Open_Profile_Viewer ? PV_Click_Glycan_Store : Click_Glycan_Store;

  for (var i = 1; i < CGS.length + 1; i++) {
    if (RegExp(CGS[i - 1]).test(LC)) {
      return i;
    }
  }

  return 0;
}
/*------------------------------reg_check関数ここまで------------------------------*/

/*------------------------------糖鎖構造を選択したときに、その糖鎖構造を糖鎖詳細画面に表示する、あるいは削除するイベント------------------------------*/
{
  //糖鎖描画画面に表示された糖鎖構造、Profile Viewerで表示されるグラフの四角形、グラフに表示される「0」の文字のいずれかを選択することで処理を行う
  $(document).on("click", ".Glycan_class , #canvas rect, #canvas text", function () {
    //ここでTissueNameを取得するための配列番号をProfileViewerかそうでないかで分ける
    //通常であれば、0→TissueID, 1→TissueName, 2→Dataなので、1が、
    //ProfileViewerであれば、0→TissueName, 1→SubTissueName, 2→Glycan(Data)なので0が代入される
    var get_Tissue_name_index = Open_Profile_Viewer ? 0 : 1;

    //組織表のtbodyタグに埋め込んだIDを取得し、その番号から組織名を取得する
    var selectID = document.getElementById(add_clone_("list_table")).childNodes[3].getAttribute("data-select");

    //SubTissueNameを先に（Profile Viewerのときだけ）取得
    var subtissue_name = Open_Profile_Viewer
      ? document.getElementById(add_clone_("list_tr") + selectID).childNodes[get_Tissue_name_index + 1].innerHTML
      : "";

    var selectTissue = "";

    //Profile Viewerの場合、空白セルを選択していることがあるので、このループにより組織名を調べる
    //選択した行から上に向かって行のIDを取得し、その1列目のTissue欄を取得。ここに文字列があればループを抜ける
    do {
      selectTissue = document.getElementById(add_clone_("list_tr") + selectID).childNodes[get_Tissue_name_index]
        .innerHTML;
      selectID--;
    } while (selectTissue === "");

    //取得した生物種名、組織名、副組織名からLCListを取得する
    var LC = Get_LCList(disabled_button_search("strings"), selectTissue, subtissue_name);

    //クリックした糖鎖の番号を格納するため、idに付いている文字列を削除して数字だけ抜き出す
    var row = this.id.replace(/\D/g, "");

    //取得した糖鎖の番号を基に、LCListからLinearCodeを取得
    LC = LC["result"]["item"][row]["code"][0];

    var i = reg_check(LC);

    if (i) {
      //選択が解除された糖鎖構造の糖鎖詳細画面上の表を削除
      //表の大枠となっているIDを取得
      var detailed_table_id = document.getElementById(add_clone_("Detailed_table"));

      if (!Open_Profile_Viewer) {
        //選択中の全糖鎖数から選択した糖鎖の配列格納番号を引く（表が逆順に出力されているため）
        //そこからさらに1を引く（lengthが最大要素番号ではなく＋1の要素数を出力するため）
        detailed_table_id.deleteRow(Click_Glycan_Store.length - i);

        //配列から特定の糖鎖構造だけを削除
        Click_Glycan_Store.splice(i - 1, 1);
      } else {
        //選択中の全糖鎖数から選択した糖鎖の配列格納番号を引く（表が逆順に出力されているため）
        //そこからさらに1を引く（lengthが最大要素番号ではなく＋1の要素数を出力するため）
        detailed_table_id.deleteRow(PV_Click_Glycan_Store.length - i);

        //配列から特定の糖鎖構造だけを削除
        PV_Click_Glycan_Store.splice(i - 1, 1);
      }
    } else {
      //Detailed_tableに糖鎖構造を大きく表示させる
      var strings = '<tr oncontextmenu="return false;" class="Detailed_class';
      strings += Open_Profile_Viewer ? '_clone" ' : '" ';
      strings += 'data-select="';
      strings += disabled_button_search("strings") + " " + selectTissue; //現在選択している生物種と組織を独自データ属性(data-select)に追加
      strings += '">';
      strings += '<td class="Detailed_td';
      strings += Open_Profile_Viewer ? '_clone" ' : '" ';
      strings += 'id="Detailed_td';
      strings += Open_Profile_Viewer ? PV_Click_Glycan_Store.length : Click_Glycan_Store.length;
      strings += '" name="';
      strings += LC;
      strings += '">';
      strings += GlycanImage.imageLC(LC.replace(filter_regexp, ""), "svg", 1.7);
      strings += '<span class="detailed_cross';
      strings += Open_Profile_Viewer ? '_clone">' : '">';
      strings += "✕</span></td></tr>";

      //配列にLinearCodeをプッシュ（左：Profile Viewerに使用する配列に格納　右：メインに使用する配列に格納）
      Open_Profile_Viewer
        ? PV_Click_Glycan_Store.push(split_and_marge(LC, false, false))
        : Click_Glycan_Store.push(split_and_marge(LC, false, false));

      $(add_clone_("#Detailed_table")).prepend(strings);
    }

    //背景色が変わった糖鎖が存在する組織と表を全て色付けする
    query_function();
  });
}
/*------------------------------LCListを検索するon("click")イベントここまで------------------------------*/

/*------------------------------グラフを作成する関数------------------------------*/
function Create_Graph_Pane(max_canvas_width) {
  var graph_area = document.getElementById("scale");

  //一度、スケールを全部削除（横棒の長さを幅いっぱいまで調整するため）
  while (graph_area.firstChild) graph_area.removeChild(graph_area.firstChild);

  graph_area.setAttribute("viewBox", "0 0 " + max_canvas_width + " 320");
  graph_area.setAttribute("width", max_canvas_width);
  graph_area.setAttribute("height", "320");

  var path = "";

  for (var i = 0; i <= 10; i++) {
    path += '<text x="';
    path += 10;
    path += '" y="';
    path += 310 - i * 30;
    path += '">' + i * 10 + "</text>";

    path += '<line x1="30" x2="';
    path += max_canvas_width;
    path += '" y1="';
    path += 305 - i * 30;
    path += '" y2="';
    path += 305 - i * 30;
    path += '" stroke="#AAAAAA"/>';
  }

  graph_area.innerHTML += path;
}
/*------------------------------Create_Graph_Pane関数ここまで------------------------------*/

/*------------------------------グラフを描画する関数------------------------------*/
function draw_graph(LC_List) {
  //LC_Listから、糖鎖構造がいくつあるかを数える
  var loop = LC_List["result"]["item"].length;

  //グラフ描画エリア（キャンバス）の全幅を決定（エリアの右端マージン＋(グラフの幅＋グラフとグラフの隙間の幅)×グラフ数）
  var max_canvas_width = 10 + (10 + 2) * loop;
  var left_margin = 30;
  max_canvas_width += left_margin;

  var graph_area = document.getElementById("canvas");

  //キャンバスの全幅と高さをここで設定
  graph_area.setAttribute("viewBox", -left_margin + " 0 " + max_canvas_width + " 320");
  graph_area.setAttribute("width", max_canvas_width);
  graph_area.setAttribute("height", "320");

  document.getElementById("all_graph").setAttribute("viewBox", "0 0 " + max_canvas_width + " 320");
  document.getElementById("all_graph").setAttribute("width", max_canvas_width);

  //既にSVGで棒グラフが作成されていたら、それを全て削除する
  while (graph_area.firstChild) graph_area.removeChild(graph_area.firstChild);

  var path = "";

  //糖鎖数ループ
  for (var i = 0; i < loop; i++) {
    //グラフの色を決める（rgb(R,G,B)）
    //色の減少度合を計算
    var color = (255 + 255) / loop;
    //青色の値を計算
    var Blue = Math.round(255 - color * i);
    //緑色の値は（最初は）255で固定
    var Green = 255;

    //もし、青色の値が0以下になったら
    if (Blue < 0) {
      //青色は0に固定し、緑色の値を計算する
      Blue = 0;
      Green -= Math.round(color * i - 255);
    }

    //計算したカラー値をここで代入
    var color_style = "rgb(0," + Green + "," + Blue + ")";
    //糖鎖のvalue値をLCListから取得
    var value = LC_List["result"]["item"][i]["value"][0];

    //棒グラフを描き始めるX座標の位置
    var draw_x = 10 + 12 * i;

    //棒グラフを描き始めるY座標の位置
    //キャンバスの高さ - 15
    var height = document.getElementById("canvas").height.baseVal.value - 15;
    var draw_y = height * (value / 102);

    //棒グラフの幅
    var rect_width = 10;

    path += value == 0 ? '<text id="graph_' : '<rect id="graph_';
    path += i;
    path += '" class="Graph" x="';
    path += draw_x;
    path += '" y="';
    path += height - draw_y;
    path += '" width="';
    path += rect_width;
    path += '" height="';
    path += draw_y;
    path += '" fill="';
    path += color_style;
    path += value == 0 ? '">0</text>' : '"/>';
  }

  graph_area.innerHTML += path;

  Create_Graph_Pane(max_canvas_width);

  //もし、画面サイズが変更されたら、ここでグラフ描画画面の大きさを変更する
  $(window).resize(function () {
    var max_table_width = document.getElementById("graph").getBoundingClientRect().width * 0.95;
    document.getElementById("graph_table").width = max_table_width;
  });
}
/*------------------------------draw_graph関数ここまで------------------------------*/

/*-------------------------------✕印クリックで糖鎖詳細画面に表示されている糖鎖を1つだけ消すイベント-----------------------------*/
$(document).on("click", ".detailed_cross , .detailed_cross_clone", function () {
  var parent_element = this.parentNode;
  var parent_name = parent_element.getAttribute("name");

  var detailed_table_id = document.getElementById(add_clone_("Detailed_table"));

  var i = reg_check(parent_name);

  //選択中の全糖鎖数から選択した糖鎖の配列格納番号を引く（表が逆順に出力されているため）
  //そこからさらに1を引く（lengthが最大要素番号ではなく＋1の要素数を出力するため）
  if (!Open_Profile_Viewer) {
    detailed_table_id.deleteRow(Click_Glycan_Store.length - i);
    //配列から特定の糖鎖構造だけを削除
    Click_Glycan_Store.splice(i - 1, 1);
  } else {
    detailed_table_id.deleteRow(PV_Click_Glycan_Store.length - i);
    //配列から特定の糖鎖構造だけを削除
    PV_Click_Glycan_Store.splice(i - 1, 1);
  }

  query_function();
});
/*-------------------------------ここまで------------------------------*/

/*-------------------------------All Clearボタンをクリックすることで、糖鎖詳細画面に表示された糖鎖を全て削除する関数------------------------------*/
function Glycan_All_Clear() {
  //選択が解除された糖鎖構造の糖鎖詳細画面上の表を全て削除
  //表の大枠となっている糖鎖詳細画面のIDを取得
  var tissue_table = document.getElementById(add_clone_("Detailed_table"));

  //全ての表を削除
  //糖鎖詳細画面に表示されている糖鎖数（行数）が0になるまで、糖鎖詳細画面の行を消去
  while (tissue_table.rows.length > 0) tissue_table.deleteRow(-1);

  //空配列を代入して、選択した要素を0にする。
  if (!Open_Profile_Viewer) {
    Click_Glycan_Store = [];
  } else {
    PV_Click_Glycan_Store = [];
  }

  query_function();
}
/*------------------------------Glycan_All_Clear関数ここまで-----------------------------*/

/*------------------------------全てのTissueをLinearCodeで横断検索をかけて組織を色付けする------------------------------*/
function query_function() {
  //その生物種が持つ組織数を数える
  var tissue = document.getElementById(add_clone_("list_table")).childNodes[3].childElementCount;

  for (var i = 0; i < tissue; i++) {
    //TissueListを取得する
    var cnt = Get_TissueList();

    //組織表からi行目のIDを取得するために、まずは行のIDの一部を取得
    var selecting_tr_id = add_clone_("list_tr");

    //クリックした行番号（から1引いたIDの持つ行）の要素を取得する
    var table = document.getElementById(selecting_tr_id + String(i));

    var tissue_name; //クリックした行の組織名を格納

    //Profile Viewerでない場合は、こちらの処理を行う
    if (!Open_Profile_Viewer) {
      //その生物種が持つi番目のtissueに存在する糖鎖数を代入
      cnt = cnt["result"]["species"][disabled_button_search("number")]["tissue"][i]["$"]["cnt"];

      tissue_name = table.childNodes[1].innerHTML; //組織名を取得

      //組織名、副組織名（なし）、その組織に存在する糖鎖数、現在のループ値を関数の引数にする
      coloring(tissue_name, "", cnt, i);
    } else {
      //先に副組織名と糖鎖数を組織表から取得
      var subtissue_name = table.childNodes[1].innerHTML;
      var cnt = table.childNodes[2].innerHTML;

      //組織名を取得するための一時的な変数に、最初にクリックした行の位置を代入
      var table_tmp = table;

      //現在のカウンタを一時的なカウンタ変数に代入
      var i_tmp = i;

      //組織名が空だった場合、ループ
      while ((tissue_name = table_tmp.childNodes[0].innerHTML) === "") {
        //i_tmpから1引いた行の要素を取得（空セルなので、文字が登場するまで上に移動する。見つかった文字が、その空セルの組織名）
        table_tmp = document.getElementById(selecting_tr_id + String(--i_tmp));
      }

      //組織名、副組織名、その組織に存在する糖鎖数、現在のループ値を関数の引数にする
      coloring(tissue_name, subtissue_name, cnt, i);
    }
  }

  function coloring(tissue_name, subtissue_name, cnt, i) {
    var find_flag = false;

    //その組織が持つ糖鎖の数だけループ
    for (var j = 0; j < cnt; j++) {
      //i番目のSVG画像のIDを取得（Profile Viewerの場合はnullを代入）
      var svg = !Open_Profile_Viewer ? document.getElementById(Number(i)) : null;

      //LCListを取得する。引数の2つ目は、表のi行と1列目（TissueName）から、TissueNameを取得
      var code = Get_LCList(disabled_button_search("strings"), tissue_name, subtissue_name);
      if (code == undefined || code == null) {
        console.log("Undefined code");
        return;
      }
      code = code["result"]["item"][j]["code"][0];

      var check = reg_check(code);

      if (check) {
        //その組織に1つでも選択した糖鎖構造が見つかれば、すぐにbreak（組織表と組織画像の色を変更するだけであるため）
        find_flag = true;
        break;
      }
    }

    //もし、選択した組織と同じ組織なら、その組織を黄色に色付けする（現在のループ値と、クリックした行のID値（をtbodyタグに埋め込んだ数値）が一緒なら選択された組織とみなす）
    //さらに、選択した糖鎖構造の背景色を桃色にする
    if (i == document.getElementById(add_clone_("list_table")).childNodes[3].getAttribute("data-select")) {
      //組織表と組織画像の色を変更
      change_color(svg, table, "yellow");
      //糖鎖描画画面に表示されている糖鎖構造の背景（と、Profile Viewerの場合グラフ）の色を変更
      change_Glycan_background_color(disabled_button_search("strings"), tissue_name, subtissue_name, cnt);
    }

    //もし選択済みの糖鎖が見つかれば組織を桃色にする
    else if (find_flag) {
      change_color(svg, table, "pink");
    }

    //見つからなければ組織を白色にする
    else {
      change_color(svg, table, "white");
    }
  }
}
/*------------------------------query_functionここまで------------------------------*/

/*------------------------------それぞれのモーダルウィンドウを表示するための処理------------------------------*/
$(function () {
  // それぞれのボタンをクリック時に、fadeInメソッドでHTML要素を表示する
  $(document).on("click", "#Data_source , #Glycan_search , #Profile_input, #Glycan_search_clone", function () {
    //モーダルウィンドウを表示させたときに、メイン画面、あるいはProfile Viewerのスクロールを禁止する
    document.body.style.overflow = "hidden";

    //eleidに「#押したボタンのID_window_text」の文字列を代入
    var eleid = "#" + this.getAttribute("id") + "_window_text";

    //もしProfile Viwerだったら、通常よりもさらにスタックレベルが高いオーバーレイを表示
    //スタックレベル：表示させる順番。CSSのz-index値で決定できる。数値が高いほど、手前に表示される。
    //.overlay：スタックレベル1　.PV_overlay：スタックレベル2
    Open_Profile_Viewer ? $(".PV_overlay").fadeIn() : $(".overlay").fadeIn();
    $(eleid).fadeIn();

    //もしGlycan_searchがクリックされたら

    if (eleid === add_clone_("#Glycan_search") + "_window_text") {
      //テキストボックスを空白に、糖鎖描画画面を空に、inputボタンの押下を無効にする
      $(add_clone_("#input_LC")).val("");
      $(add_clone_("#drawing")).empty();
      Open_Profile_Viewer
        ? $("#click_input_LC_button_clone").prop("disabled", true)
        : $(".input").prop("disabled", true);
    }

    //もしProfile inputがクリックされたら
    else if (eleid === "#Profile_input_window_text") {
      //同じファイルをモーダルウィンドを閉じた後でも開けるようにするために、nullに設定（changeイベントが発火しないため）
      document.getElementById("file_open").value = null;
      //テキストボックスを空にし、inputボタンを押下不可に、さらに表のスクロールを無効にする
      document.getElementById("profile_name").value = null;
      $("#click_profile_button").prop("disabled", true);
      $("#profile_table>tbody").css("overflow-y", "hidden");

      //空テーブルの作成のため、一旦表を削除
      var table_id = document.getElementById("profile_table");
      while (table_id.rows.length > 1) table_id.deleteRow(-1);

      var string = "";

      //空テーブルの作成
      for (var i = 0; i < 14; i++) {
        string += "<tr>";

        for (var j = 0; j < 5; j++) {
          string += "<td>&nbsp;</td>";
        }

        string += "</tr>";
      }

      $(table_id).append(string);
    }

    locateCenter(); // => モーダルウィンドウを中央配置するための初期値を設定する
    Open_Profile_Viewer ? $("#Profile_Viewer").resize(locateCenter) : $(window).resize(locateCenter); // => ウィンドウのリサイズに合わせて、モーダルウィンドウの配置を変える
  });

  // 「Close」ボタン（または「Cancel」ボタン、あるいはGlycan_searchの場合は「input」ボタン）、右上のバツ印をクリック時に、fadeOutメソッドでHTML要素を非表示にする
  $(document).on("click", ".close , .cross , .input", function () {
    $(
      ".overlay, #Data_source_window_text , #Glycan_search_window_text, #Profile_input_window_text, #Profile_Viewer_window_text"
    ).fadeOut();
    //モーダルウィンドウが閉じられたときに、メイン画面のスクロールを可能にする
    document.body.style.overflow = "visible";
  });

  //上の処理にProfile Viwerで使用するモーダルウィンドウを加えてしまうとProfile Viewerごと閉じてしまうので別処理にする
  //Profile Viewerで使用されるモーダルウィンドウは、Glycan Searchと右クリックメニューから選択できる「link to」のエラーメッセージのみ
  $(document).on("click", ".PV_Glycan_Search_cross , .PV_close , #click_input_LC_button_clone", function () {
    $(".PV_overlay, #Glycan_search_clone_window_text").fadeOut();
    //モーダルウィンドウが閉じられたときに、Profile Viewerのスクロールを可能にする
    document.getElementById("Profile_Viewer").style.overflow = "visible";
  });
});
/*------------------------------ここまで------------------------------*/

/*------------------------------モーダルウィンドウの配置位置を設定する------------------------------*/
// モーダルウィンドウを中央配置するための配置場所を計算する関数
function locateCenter() {
  var w = $(window).width();
  var h = $(window).height();

  //Data SourceウィンドウとGlycan SearchウィンドウとProfile inputウィンドウの高さと幅を取得
  var cw = $(
    "#Data_source_window_text , #Glycan_search_window_text , #Profile_input_window_text, #Glycan_search_clone_window_text"
  ).outerWidth();
  var ch = $(
    "#Data_source_window_text , #Glycan_search_window_text , #Profile_input_window_text, #Glycan_search_clone_window_text"
  ).outerHeight();

  //糖鎖詳細画面の右クリックメニューから、Glycan_searchでインプットした糖鎖構造をCFGかGTCで検索しようとしたときのエラーウィンドウの高さと幅を取得
  var cw_error = $("#not_have_number").outerWidth();
  var ch_error = $("#not_have_number").outerHeight();

  var cw_pv = $("#Profile_Viewer_window_text").outerWidth();
  var ch_pv = $("#Profile_Viewer_window_text").outerHeight();

  $(
    "#Data_source_window_text , #Glycan_search_window_text , #Profile_input_window_text, #Glycan_search_clone_window_text"
  ).css({
    left: (w - cw) / 2 + "px",
    top: (h - ch) / 2 + "px"
  });

  $("#not_have_number").css({
    left: (w - cw_error) / 2 + "px",
    top: (h - ch_error) / 2 + "px"
  });

  $("#Profile_Viewer_window_text").css({
    left: (w - cw_pv) / 2 + "px",
    top: (h - ch_pv) / 2 + "px"
  });
}
/*------------------------------locateCenterここまで------------------------------*/

/*------------------------------Glycan searchの画面で行われる処理------------------------------*/
function LC_drawing() {
  var table_id = document.getElementById(add_clone_("drawing"));
  var drawing_glycan;

  //テキストエリアに入力されてたLinear Code文字列を取得
  var input_LC = $(add_clone_("#input_LC")).val();

  //一旦、糖鎖構造を表示するエリアを削除
  while (table_id.rows.length > 0) table_id.deleteRow(-1);

  //エリアを再作成する
  drawing_glycan = '<tr style="border: none;"><td style="border: none;">';

  //もし、テキストエリアに入力された文字列が空白でなければ、GlycanImageでsvg画像を表示する
  if (input_LC !== "") {
    drawing_glycan += GlycanImage.imageLC(input_LC.replace(filter_regexp, ""), "svg", 1.7);
  }

  drawing_glycan += "</td></tr>";

  $(add_clone_("#drawing")).prepend(drawing_glycan);

  Open_Profile_Viewer ? $("#click_input_LC_button_clone").prop("disabled", false) : $(".input").prop("disabled", false);
}
/*------------------------------LC_drawing関数ここまで------------------------------*/

/*------------------------------Glycan searchでinputボタンが押されたときの処理------------------------------*/
$(document).on("click", "#click_input_LC_button , #click_input_LC_button_clone", function () {
  var input_LC = $(add_clone_("#input_LC")).val();
  var use_wildcard;
  var use_PMS;

  //糖鎖詳細画面に表示するための処理
  var strings = '<tr oncontextmenu="return false;" class="Detailed_class';
  strings += Open_Profile_Viewer ? '_clone" ' : '" ';
  strings += 'data-select="';
  strings += "Search";
  strings += '">';
  strings += '<td id="Detailed_td';
  strings += Open_Profile_Viewer ? PV_Click_Glycan_Store.length : Click_Glycan_Store.length;
  strings += '" name="';
  strings += input_LC;
  strings += '">';
  strings += GlycanImage.imageLC(input_LC.replace(filter_regexp, ""), "svg", 1.7);
  strings += '<span class="detailed_cross';
  strings += Open_Profile_Viewer ? '_clone">' : '>"';
  strings += "✕</span></td></tr>";

  $(add_clone_("#Detailed_table")).prepend(strings);

  //「ワイルドカードを使用する」にチェックが入っているならtrue
  use_wildcard = $("[name=wild_card]:checked").val() === "w_check";

  //「PMSを使用する」にチェックが入っているならtrueに、入っていないならfalseに
  use_PMS = $("[name=PMS]:checked").val() === "pms_check";

  Open_Profile_Viewer
    ? PV_Click_Glycan_Store.push(split_and_marge(input_LC, use_wildcard, use_PMS))
    : Click_Glycan_Store.push(split_and_marge(input_LC, use_wildcard, use_PMS));

  query_function();
});
/*------------------------------on("click")イベントここまで------------------------------*/

/*------------------------------splitした時などに生じる余計な文字を削除する関数------------------------------*/
function isNotEmptyItem(element) {
  if (element === "" || element === undefined || element === "\n" || element === "\r" || element === "\r\n") {
    return false;
  }
  return true;
}
/*------------------------------isNotEmptyItem関数ここまで------------------------------*/

/*------------------------------Linear Codeを正規表現文字列に置き換える処理------------------------------*/
function split_and_marge(input_LC, use_wildcard, use_PMS) {
  var search_strings = "";

  //ワイルドカードを使用しないなら、input_LCを正規表現用にエスケープしてそのまま返す
  if (use_wildcard == false) {
    search_strings = input_LC.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&").replace(/\"/g, "");
  } else {
    //インプットされた文字列を正規表現に従って分割、フィルターで空要素やundefinedを削除
    var splits = input_LC.split(/(,)|([A-Z]{1,2})|([?][?])|([ab?][\d?])|([)(])/g).filter(isNotEmptyItem);

    //糖鎖の位置が奇数進行かどうか（true->奇数）
    //ただし、配列の最初の番号は0からだが、1番目から数えるとしているため奇数進行である
    var odd = true;
    var comma = 0;
    var i = 0;

    while (splits.length > i) {
      //()の場合、進行をスイッチさせて、文字を代入する（括弧は特殊文字なので、バックスラッシュでエスケープさせるが、「"」をエスケープさせてしまうので二重にしている）
      if (splits[i] === ")" || splits[i] === "(") {
        search_strings += "\\" + splits[i];
        odd = !odd;
      }

      //??の場合の処理
      else if (/[?][?]/.test(splits[i])) {
        //糖鎖の正規表現に関する処理(XORによる分岐)
        if ((odd && !(i % 2)) || (!odd && i % 2)) {
          search_strings += "([A-Z]{1,2})";
        }

        //結合の正規表現に関する処理
        else {
          search_strings += "([ab?][\\d?])";
        }
      }

      //糖鎖結合に関する正規表現（??は前のif文で除かれている）
      else if (/([ab?][\d?])/.test(splits[i])) {
        //1文字目が?のときと、2文字目が?のときとで正規表現を変える
        search_strings +=
          splits[i].charAt(0) === "?"
            ? "([ab?][" + splits[i].charAt(1) + "?])"
            : "([" + splits[i].charAt(0) + "?][" + splits[i].charAt(1) + "?])";
      }

      //それ以外の文字（完全な糖鎖略号、完全な結合情報、,（コンマ））
      else {
        /*if( splits[i] === "," )
                 {
                 search_strings += (!comma) ? ",\\([" : ",";
                 comma++;
                 }

                 else if( comma )
                 {

                 }*/

        search_strings += splits[i];
      }

      i++;
    }
  }

  //PMSにチェックが入っていないなら前方に「^」後方に「＄」をつけて、前にも後ろにも文字が続かないようにする
  if (use_PMS == false) {
    search_strings = "^" + search_strings + "$";
  }

  return search_strings;
}
/*------------------------------split_and_marge関数ここまで------------------------------*/

/*------------------------------右クリックメニューを表示する処理------------------------------*/
var LC_tmp;
$(document).on("contextmenu", ".Detailed_class , .Detailed_class_clone", function (e) {
  var row = $(this).closest("tr").index(); //新しく追加されたものが0。そこから下へ＋される
  row = Open_Profile_Viewer ? PV_Click_Glycan_Store.length - 1 - row : Click_Glycan_Store.length - 1 - row; //番号を入れ替えて下を0にする。
  LC_tmp = row;

  //HTML要素のjs-contextmenuを取得
  var myContextMenu = document.getElementById("js-contextmenu");
  //現在のページの座標を取得する
  var posX = e.pageX - window.scrollX;
  var posY = e.pageY - window.scrollY;

  //コンテキストメニューの表示位置のマージンを設定する
  myContextMenu.style.left = posX + "px";
  myContextMenu.style.top = posY + "px";

  myContextMenu.classList.add("show");

  //コンテキストメニューを表示する
  document.body.addEventListener("click", function () {
    if (myContextMenu.classList.contains("show")) {
      myContextMenu.classList.remove("show");
    }
  });
});
/*------------------------------on("contextmenu")ここまで------------------------------*/

/*------------------------------Linear Codeをコピーするための前処理を行う------------------------------*/
function LC_Copy() {
  //右クリックした糖鎖構造のIDを取得（正確には糖鎖詳細画面に表示されている順番を表すID）
  var id = "Detailed_td" + LC_tmp;
  var LC = document.getElementById(id).getAttribute("name"); //IDからnameを指定して、Linear Codeを取得する

  //取得したLinear Codeを基に、クリップボードにコピーするための処理を行う
  copyTextToClipboard(LC);
}
/*------------------------------LC_Copy関数ここまで------------------------------*/

/*------------------------------コピーするための文字列を、ここでクリップボードにコピーする------------------------------*/
function copyTextToClipboard(textVal) {
  // テキストエリアを用意する
  var copyFrom = document.createElement("textarea");
  // テキストエリアへ値をセット
  copyFrom.textContent = textVal;

  // bodyタグの要素を取得
  var bodyElm = document.getElementsByTagName("body")[0];
  // 子要素にテキストエリアを配置
  bodyElm.appendChild(copyFrom);

  // テキストエリアの値を選択
  copyFrom.select();
  // コピーコマンド発行
  var retVal = document.execCommand("copy");
  // 追加テキストエリアを削除
  bodyElm.removeChild(copyFrom);

  $(function () {
    //コピーアラートの出現させる要素を追加する
    Open_Profile_Viewer
      ? $("#Profile_Viewer").append('<div id="alert" class="grad">Linear Code® Copied!</div>')
      : $("body").append('<div id="alert" class="grad">Linear Code® Copied!</div>');

    //コピーアラートの大きさを計算
    var ah = $("#alert").height();
    var aw = $("#alert").width();
    var top = ah / 0.4;
    var left = $(window).width() / 2 - aw / 2;

    //計算したコピーアラートの位置をCSSに追加し、透過度を0（透明）から1（不透明）に300msでアニメーション表示させる
    $("#alert").css({ top: top, left: left, opacity: 0 }).animate({ opacity: 1 }, 300);

    setTimeout(function () {
      //1000ms(1s)間メッセージを確認できるようにした後、透過度を0（透明）に300msかけて戻す
      $("#alert").animate({ opacity: 0 }, 300, function () {
        //透過度が0（透明）になった後、コピーアラートを出現させるために追加した要素を削除する。
        $(this).remove();
      });
    }, 1000);
  });

  return retVal;
}
/*------------------------------copyTextToClipboard関数ここまで------------------------------*/

function getLCFromGTC(species, tissue, gtcID) {
  //var selector = { CFG:'carbID', GTC:'AN' };
  //生物種名と組織名からLCListを取得する
  var getLC = Get_LCList(species, tissue, "");

  //リンク先（初期値はundefined）
  var LCstring = "undefined";

  if (getLC == null) {
    console.log("Null LC for " + species + " and " + tissue);
    return LCstring;
  }

  for (var i = 0 in getLC["result"]["item"]) {
    //もし、糖鎖構造の持つLinear CodeとLCList上のLinear Codeが一致したら（糖鎖構造が見つかったら）
    if (gtcID === getLC["result"]["item"][i]["AN"][0]) {
      //link_idにCFGかGTCの番号を代入し、ループを抜ける
      LCstring = getLC["result"]["item"][i]["code"][0];
      break;
    }
  }
  return LCstring;
}
function getLinkFromLCcode(species, tissue, code, type) {
  var selector = { CFG: "carbID", GTC: "AN" };
  //生物種名と組織名からLCListを取得する
  var getLC = Get_LCList(species, tissue, "");

  //リンク先（初期値はundefined）
  var link_id = "undefined";

  for (var i = 0 in getLC["result"]["item"]) {
    //もし、糖鎖構造の持つLinear CodeとLCList上のLinear Codeが一致したら（糖鎖構造が見つかったら）
    if (code === getLC["result"]["item"][i]["code"][0]) {
      //link_idにCFGかGTCの番号を代入し、ループを抜ける
      link_id = getLC["result"]["item"][i][selector[type]][0];
      break;
    }
  }
  return link_id;
}

/*------------------------------右クリックメニューの「link to」をクリックしたときの処理------------------------------*/
function Linking(type) {
  var tmp = "Detailed_td" + LC_tmp;
  var code;

  //右クリックメニューを表示させた糖鎖のID（糖鎖詳細画面における行数）を取得し、そのIDを持つ糖鎖構造からname値（Linear Code）を取得する
  var test = document.getElementById(tmp);
  code = test.getAttribute("name");

  //tmp（右クリックした糖鎖構造が持つtdタグ）の親ノード（trタグ）を取得し、糖鎖構造を糖鎖詳細画面に表示した際に付与したIDを取得する
  tmp = document.getElementById(tmp).parentNode;
  tmp = tmp.getAttribute("data-select");

  //この後、もし番号を持っていなかった場合にエラーメッセージを表示するために、空のp要素を取得
  var error = document.getElementById("not_have_number").childNodes[3];

  //もし、Glycan Searchを使用して糖鎖詳細画面に表示したものだったら
  //（もし、糖鎖詳細画面に表示したtdタグの中にSearchという文字が存在するなら）
  if (tmp.indexOf("Search") != -1) {
    //次のエラーメッセージをモーダルウィンドウで表示する（右クリックメニューでCFGを選択したなら前者、GTCを選択したなら後者が表示される）
    error.innerHTML =
      type === "CFG" ? "Search query dose not have CarbID!" : "Search query dose not have Accession Number!";
    modal();
  } else {
    //糖鎖構造を持つ、生物種名と組織名をスペースで分割して配列にする
    var item = tmp.split(/\s/);

    //Large BowelやLymph Nodeなどは間にスペースが入っているため、上のコードで分割されてしまうがそれを修正
    //itemの要素数が2よりも多ければ（通常であれば0→生物種名 1→組織名の2要素で収まる）
    if (item.length > 2) {
      //item[1]に分割されてしまった組織名を修復して代入
      item[1] = item[1] + " " + item[2];
      //末尾（item[2]）を削除
      item.pop();
    }

    var link_id = getLinkFromLCcode(item[0], item[1], code, type);

    //代入されているlink_idの値によって処理を変える
    switch (link_id) {
      case "undefined":
        {
          //undefinedなら、その糖鎖構造が何らかの原因で見つからなかったため、次のエラーメッセージを表示
          error.innerHTML = "This glycan is not found!";
          modal();
        }
        break;

      case "null":
        {
          //nullなら、その糖鎖構造にはリンク先となる番号が付与されていないため、次のエラーメッセージを表示
          error.innerHTML =
            type === "CFG" ? "This structure does not have CarbID!" : "This structure does not have Accession Number!";
          modal();
        }
        break;

      default: {
        var URL = "";
        var CFG_URL =
          "http://www.functionalglycomics.org/glycomics/CarbohydrateServlet?pageType=view&view=view&operationType=view&carbId=";
        var GTC_URL = "https://glytoucan.org/Structures/Glycans/";

        //それ以外の場合は番号が付与されているので、取得した番号をCFGあるいはGTCのURLにパラメータとして結合させ、別タブでリンクを開く
        URL = type === "CFG" ? CFG_URL + link_id : GTC_URL + link_id;
        window.open(URL);
      }
    }
  }
}
/*------------------------------Linking関数ここまで------------------------------*/

/*------------------------------エラーメッセージをモーダルウィンドウで表示する処理------------------------------*/
function modal() {
  Open_Profile_Viewer ? $(".PV_overlay").fadeIn(100) : $(".overlay").fadeIn(100);
  $("#not_have_number").fadeIn(100); //エラーメッセージを表示

  //スクロールをできなくする
  document.body.style.overflow = "hidden";

  //✕ボタン、closeボタンが押されたらウィンドウを非表示に戻し、スクロールできるようにする
  $(".close , .cross").on("click", function () {
    $(".overlay, #not_have_number").fadeOut(100);
    document.body.style.overflow = "visible";
  });

  locateCenter(); // => モーダルウィンドウを中央配置するための初期値を設定する
  $(window).resize(locateCenter);
}
/*------------------------------modal関数ここまで------------------------------*/

/*------------------------------profile inputで読み込んだファイルの表示処理------------------------------*/
function file_open() {
  var profile = [];
  var header = {};
  var used_species = [];

  //「Browser...」ボタンクリックによって、非表示になっているファイルアップロードボタンをクリックする
  $("#file_open").click();

  //ファイル名が変わったらイベントを実行
  document.getElementById("file_open").addEventListener("change", function () {
    //アップロードされたファイル情報を取得
    var item = event.target.files[0];

    //ファイル情報の読み込み
    var reader = new FileReader();
    reader.readAsText(item);

    //取得したファイル名を、横のテキストボックスに動的に入力
    document.getElementById("profile_name").value = item.name;

    reader.onload = function () {
      var str = reader.result;

      //改行コードで取得したファイルを分割して、1行ずつ配列に保存
      var split = str.split(/(\n|\r|\r\n)/g).filter(isNotEmptyItem);

      //1行ずつタブで分割したものをprofile配列に代入
      for (var i = 0; i < split.length; i++) {
        profile[i] = split[i].split("\t");
      }

      //一度、Profile input内の仮の表を全部削除
      var table_id = document.getElementById("profile_table");
      while (table_id.rows.length > 1) table_id.deleteRow(-1);

      //配列の作成
      var header_name = ["linear_code", "species", "tissue", "subtissue", "value"];

      //ファイルの中に記載されているヘッダー（正規表現によって多少の表記ゆれは許される）を格納
      //ヘッダーに格納される数値が、ファイルの中で何番目のカラムかを表す（これにより、ヘッダーの順番がProfile Viewerと違ったとしても、修正できる）
      for (var i = 0; i < profile[0].length; i++) {
        //許される表記ゆれ→Linear Code, LINEAR CODE, Linearcode, LinearCode, LiNeArCoDe etc.
        if (/linear\s?code/i.test(profile[0][i])) {
          header["linear_code"] = i;
        }

        //許される表記ゆれ→Species, SPECIES, SPECies etc.
        else if (/species/i.test(profile[0][i])) {
          header["species"] = i;
        }

        //許される表記ゆれ→Tissue, TISSUE, TissUe, tissues etc. ^によりsubtissueはヒットしないようにしてある
        else if (/^tissue/i.test(profile[0][i])) {
          header["tissue"] = i;
        }

        //許さるれる表記ゆれ→sub tissue, SUB TISSUE, SUBtissue, SubTissue, Sub tissues etc.
        else if (/sub\s?tissue/i.test(profile[0][i])) {
          header["subtissue"] = i;
        }

        //許される表記ゆれ→value, VALUE, Value, Values etc.
        else if (/value/i.test(profile[0][i])) {
          header["value"] = i;
        }
      }

      var strings = "";

      //Profile input内の表を新規作成
      for (var i = 1; i < profile.length; i++) {
        strings += "<tr>";

        for (var j = 0; j < profile[i].length; j++) {
          strings +=
            '<td><div style="width:100px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; padding: 0px;">';
          strings += profile[i][header[header_name[j]]];
          strings += "</div></td>";
        }

        strings += "</tr>";

        //もし、新しい生物種がプロファイル内に登場したら、配列にその生物種名をプッシュ
        if (used_species.indexOf(profile[i][header["species"]]) == -1) {
          used_species.push(profile[i][header["species"]]);
        }
      }

      //上記で作成した表を表示させる
      $("#profile_table").prepend(strings);
      //表のスクロールをオンにする
      $("#profile_table>tbody").css("overflow-y", "scroll");
      //inputボタンを押下できるようにする
      $("#click_profile_button").prop("disabled", false);
    };
  });
  /*------------------------------file_open関数ここまで------------------------------*/

  /*------------------------------Profile Viewerの開始処理------------------------------*/
  $(document)
    .off("click", "#click_profile_button")
    .on("click", "#click_profile_button", function () {
      Open_Profile_Viewer = true;

      //inputボタンが押されたらProfile Viewerモーダルウィンドウを表示し、Profile inputモーダルウィンドウを閉じる
      $(".overlay").fadeIn();
      $("#Profile_input_window_text").fadeOut();
      $("#Profile_Viewer_window_text").fadeIn();

      saved_profile(profile, used_species, header); //プロファイルを生物種別にローカルストレージに保存する関数
      make_species_button(used_species); //生物種のボタンを作成
      Create_TissueList_Table(); //Profile Viewerのプロファイル表を作成
      Create_Graph_Pane(document.getElementById("all_graph").width.baseVal.value);

      //コンテキストメニューの編集（link toの部分を非表示にする）
      document.getElementById("CFG").style.display = "none";
      document.getElementById("GTC").style.display = "none";
    });
  /*------------------------------ここまで------------------------------*/

  /*------------------------------Profile Viewerの終了処理------------------------------*/
  $(document).one("click", ".PV_cross", function () {
    //一度Profile Viewerを消去し、デフォルトの状態のProfile Viewerを付ける
    $("#Default_Profile_Viewer").remove();
    $("#Profile_Viewer").append('<div id="Default_Profile_Viewer"></div>');
    $("#Default_Profile_Viewer").append(default_profile_viewer);

    Session_delete(); //保存してあったファイルをsessionStorage上から消去

    //Profile Viewerが開いているかどうかの判定を偽にする
    Open_Profile_Viewer = false;

    //PV_Click_Glycan_Store配列の中身を空にする
    PV_Click_Glycan_Store = [];

    //コンテキストメニューの編集（link toの部分を表示する）
    document.getElementById("CFG").style.display = "block";
    document.getElementById("GTC").style.display = "block";

    //メイン画面のスクロールを可能にする
    document.body.style.overflow = "auto";

    $(".overlay, #Profile_Viewer_window_text").fadeOut();
  });
}
/*------------------------------one("click")イベントここまで------------------------------*/

/*------------------------------読み込んだユーザープロファイルをJSON形式にしてsessionStorageに保存する関数------------------------------*/
function saved_profile(profile, used_species, header) {
  //TissueListの仮組みを作成
  var user_tissue_list = {
    result: {
      species: []
    }
  };

  //LCListの仮組みを作成
  var user_lc_list = [];

  for (var i = 1; i < profile.length; i++) {
    var check = 0;
    var TissueList_profile_strings = [
      profile[i][header["species"]],
      profile[i][header["tissue"]],
      profile[i][header["subtissue"]]
    ];
    var LCList_profile_strings = [profile[i][header["linear_code"]], profile[i][header["value"]]];

    for (var j = 1; j < i; j++) {
      var pass_profile_string = [
        profile[j][header["species"]],
        profile[j][header["tissue"]],
        profile[j][header["subtissue"]]
      ];

      //生物種名、組織名、サブ組織名がすべて一致したら
      if (TissueList_profile_strings.toString() === pass_profile_string.toString()) {
        check = check | 4; // bbb|100 = 100    3bitが1になる
      }

      //生物種名と組織名の2つが一致したら
      else if (
        TissueList_profile_strings[0] === pass_profile_string[0] &&
        TissueList_profile_strings[1] === pass_profile_string[1]
      ) {
        check = check | 2; // bbb|010 = 010    2bitが1になる
      }

      //生物種名のみ一致したら
      else if (TissueList_profile_strings[0] === pass_profile_string[0]) {
        check = check | 1; // bbb|001 = 001    3bitが1になる
      }

      //生物種も一致しなければ
      else {
        check = check | 0; // bbb&000 = 000    bitは全て0
      }
    }

    //breakをわざと抜くことで、フォールスルーを実行
    //TissueListの作成
    switch (check) {
      case 0: {
        /*全て追加*/
        //仮組みを追加
        var species_length = user_tissue_list["result"]["species"].length;

        user_tissue_list["result"]["species"][species_length] = { $: { id: null, species_name: null }, tissue: [] };

        //仮組みにデータを追加
        user_tissue_list["result"]["species"][species_length]["$"]["id"] = String(
          used_species.indexOf(TissueList_profile_strings[0]) + 1
        ); //生物種のIDを追加
        user_tissue_list["result"]["species"][species_length]["$"]["species_name"] = TissueList_profile_strings[0]; //生物種の名前を追加
      }

      case 1: {
        /*tisssueに新しいtissueの情報を追加*/
        //仮組みを追加
        var species_number =
          user_tissue_list["result"]["species"][used_species.indexOf(TissueList_profile_strings[0])]["$"]["id"] - 1;
        var tissue_length = user_tissue_list["result"]["species"][species_number]["tissue"].length;

        user_tissue_list["result"]["species"][species_number]["tissue"][tissue_length] = {
          $: { id: null, tissue_name: null },
          subtissue: []
        };

        //仮組みにデータを追加
        user_tissue_list["result"]["species"][species_number]["tissue"][tissue_length]["$"]["id"] = String(
          tissue_length + 1
        ); //組織のIDを追加
        user_tissue_list["result"]["species"][species_number]["tissue"][tissue_length]["$"]["tissue_name"] =
          TissueList_profile_strings[1]; //組織の名前を追加
      }

      case 2:
      case 3: {
        /*subtissueに新しいsubtissueを追加*/
        //仮組みを追加
        var species_number =
          user_tissue_list["result"]["species"][used_species.indexOf(TissueList_profile_strings[0])]["$"]["id"] - 1;
        var tissue_number = user_tissue_list["result"]["species"][species_number]["tissue"].filter(function (item) {
          if (item["$"]["tissue_name"] === TissueList_profile_strings[1]) return true;
        });
        tissue_number = tissue_number[0]["$"]["id"] - 1;
        var subtissue_length =
          user_tissue_list["result"]["species"][species_number]["tissue"][tissue_number]["subtissue"].length;

        user_tissue_list["result"]["species"][species_number]["tissue"][tissue_number]["subtissue"][subtissue_length] =
          { $: { id: null, cnt: null, subtissue_name: null } };

        //仮組みにデータを追加
        user_tissue_list["result"]["species"][species_number]["tissue"][tissue_number]["subtissue"][subtissue_length][
          "$"
        ]["id"] = String(subtissue_length + 1); //サブ組織のIDを追加
        user_tissue_list["result"]["species"][species_number]["tissue"][tissue_number]["subtissue"][subtissue_length][
          "$"
        ]["subtissue_name"] = TissueList_profile_strings[2]; //サブ組織の名前を追加
        user_tissue_list["result"]["species"][species_number]["tissue"][tissue_number]["subtissue"][subtissue_length][
          "$"
        ]["cnt"] = String(0); //サブ組織の数をカウント（case 4で1カウントするのでここでは0を代入している）
      }

      default: //case 4,5,6,7:
      {
        /*subtissueのカウンターを1プラス*/
        var species_number =
          user_tissue_list["result"]["species"][used_species.indexOf(TissueList_profile_strings[0])]["$"]["id"] - 1;
        var tissue_number = user_tissue_list["result"]["species"][species_number]["tissue"].filter(function (item) {
          if (item["$"]["tissue_name"] === TissueList_profile_strings[1]) return true;
        });
        tissue_number = tissue_number[0]["$"]["id"] - 1;
        var subtissue_number = user_tissue_list["result"]["species"][species_number]["tissue"][tissue_number][
          "subtissue"
        ].filter(function (item) {
          if (item["$"]["subtissue_name"] === TissueList_profile_strings[2]) return true;
        });
        subtissue_number = subtissue_number[0]["$"]["id"] - 1;

        /*仮組みにデータを追加*/
        //カウンターをプラス
        user_tissue_list["result"]["species"][species_number]["tissue"][tissue_number]["subtissue"][subtissue_number][
          "$"
        ]["cnt"]++;
      }
    }

    //LCListの作成

    var list_length = user_lc_list.length;

    //全て一致していなければ
    if ((check & 4) == 0) {
      //仮組みを追加
      user_lc_list[list_length] = { query_parameter: {}, result: { item: [{}] } };

      user_lc_list[list_length]["query_parameter"] = { id_tissue: [], id_subtissue: [], id_species: [] };
      user_lc_list[list_length]["result"]["item"][user_lc_list[list_length]["result"]["item"].length - 1] = {
        code: [],
        value: []
      };

      //仮組みにデータを追加
      user_lc_list[list_length]["query_parameter"]["id_tissue"][0] =
        user_tissue_list["result"]["species"][species_number]["tissue"][tissue_length]["$"]["id"];
      user_lc_list[list_length]["query_parameter"]["id_subtissue"][0] =
        user_tissue_list["result"]["species"][species_number]["tissue"][tissue_number]["subtissue"][subtissue_length][
          "$"
        ]["id"];
      user_lc_list[list_length]["query_parameter"]["id_species"][0] =
        user_tissue_list["result"]["species"][species_length]["$"]["id"];

      user_lc_list[list_length]["result"]["item"][user_lc_list[list_length]["result"]["item"].length - 1]["code"][0] =
        LCList_profile_strings[0].replace(/(\"|\')/g, ""); //ここでダブルクォーテーションとシングルクォーテーションを取り除く（この後、Linear Codeをタグ上にnameとして載せたときにエラーの原因となるため）
      user_lc_list[list_length]["result"]["item"][user_lc_list[list_length]["result"]["item"].length - 1]["value"][0] =
        LCList_profile_strings[1];
    }

    //全て一致していれば
    else {
      var index = 0;

      while (index < user_lc_list.length) {
        if (
          user_lc_list[index]["query_parameter"]["id_tissue"][0] === String(tissue_number + 1) &&
          user_lc_list[index]["query_parameter"]["id_subtissue"][0] === String(subtissue_number + 1) &&
          user_lc_list[index]["query_parameter"]["id_species"][0] === String(species_number + 1)
        ) {
          break;
        }

        index++;
      }

      user_lc_list[index]["result"]["item"].push({ code: [], value: [] });

      user_lc_list[index]["result"]["item"][user_lc_list[index]["result"]["item"].length - 1]["code"][0] =
        LCList_profile_strings[0].replace(/(\"|\')/g, "");
      user_lc_list[index]["result"]["item"][user_lc_list[index]["result"]["item"].length - 1]["value"][0] =
        LCList_profile_strings[1];
    }
  }

  //完成したTissueListをsessionStorageに保存
  sessionStorage.setItem("UserProfile_TissueList", JSON.stringify(user_tissue_list));

  //完成したLCListをsessionStorageに保存
  for (var k = 0; k < user_lc_list.length; k++) {
    //ファイルネームの作成（形式：生物種名-組織名-サブ組織名）
    var file_name =
      user_tissue_list["result"]["species"][user_lc_list[k]["query_parameter"]["id_species"][0] - 1]["$"][
        "species_name"
      ];
    file_name +=
      "-" +
      user_tissue_list["result"]["species"][user_lc_list[k]["query_parameter"]["id_species"][0] - 1]["tissue"][
        user_lc_list[k]["query_parameter"]["id_tissue"][0] - 1
      ]["$"]["tissue_name"];
    file_name +=
      user_tissue_list["result"]["species"][user_lc_list[k]["query_parameter"]["id_species"][0] - 1]["tissue"][
        user_lc_list[k]["query_parameter"]["id_tissue"][0] - 1
      ]["subtissue"][user_lc_list[k]["query_parameter"]["id_subtissue"][0] - 1]["$"]["subtissue_name"] !== ""
        ? "-" +
          user_tissue_list["result"]["species"][user_lc_list[k]["query_parameter"]["id_species"][0] - 1]["tissue"][
            user_lc_list[k]["query_parameter"]["id_tissue"][0] - 1
          ]["subtissue"][user_lc_list[k]["query_parameter"]["id_subtissue"][0] - 1]["$"]["subtissue_name"]
        : "";

    sessionStorage.setItem("UserProfile_" + file_name, JSON.stringify(user_lc_list[k]));
  }
}
/*------------------------------saved_profile関数ここまで------------------------------*/

/*------------------------------生物種切り替えボタンを動的に作成する関数------------------------------*/
function make_species_button(used_species) {
  //生物種の数だけ切り替えボタンを作成
  for (var i = 0; i < used_species.length; i++) {
    var button = "<button id=";
    button += used_species[i];
    button += " name=";
    button += used_species[i];
    //button += ' onclick="species_branch(this.getAttribute("name"),true)"';
    button += ' onclick="species_branch(this,true)"';

    //最初の生物種は押されている状態を作る
    if (i == 0) {
      button += ' disabled="disabled"';
    }

    button += ">";
    button += used_species[i];
    button += "</button>";

    $("#button_group_clone").append(button);
  }
}
/*------------------------------make_species_button関数ここまで------------------------------*/

/*------------------------------どのボタンが押されているか（=disableになっているか）を調べる関数------------------------------*/
function disabled_button_search(query) {
  var button_group_id = add_clone_("button_group");
  var all_children = document.getElementById(button_group_id).children;

  for (var i = 0; i < all_children.length; i++) {
    if (all_children[i].disabled) {
      //文字列を返すか番号を返すか
      return query === "strings" ? all_children[i].name : i;
    }
  }

  return query === "strings" ? "Human" : 0;
}
/*------------------------------disabled_button_searchここまで------------------------------*/

/*------------------------------保存したユーザープロファイルを削除する関数------------------------------*/
function Session_delete() {
  var user_tissue_list = Get_TissueList();
  var file_name = [];

  //ユーザプロファイルから作成されたLCListの消去
  //生物種の数ループ
  for (var i = 0; i < user_tissue_list["result"]["species"].length; i++) {
    //生物種の名前を代入
    file_name[0] = user_tissue_list["result"]["species"][i]["$"]["species_name"] + "-";

    //その生物種の持つ組織の数ループ
    for (var j = 0; j < user_tissue_list["result"]["species"][i]["tissue"].length; j++) {
      //組織の名前を代入
      file_name[1] = user_tissue_list["result"]["species"][i]["tissue"][j]["$"]["tissue_name"];

      //その生物種の組織が持つサブ組織の数ループ
      for (var k = 0; k < user_tissue_list["result"]["species"][i]["tissue"][j]["subtissue"].length; k++) {
        //もし、サブ組織が見つかったら（1つ目が "" じゃなかったら）副組織の名前を代入
        file_name[2] =
          user_tissue_list["result"]["species"][i]["tissue"][j]["subtissue"][k]["$"]["subtissue_name"] === ""
            ? ""
            : "-" + user_tissue_list["result"]["species"][i]["tissue"][j]["subtissue"][k]["$"]["subtissue_name"];

        sessionStorage.removeItem("UserProfile_" + file_name[0] + file_name[1] + file_name[2]);
      }
    }
  }

  //TissueListの消去
  sessionStorage.removeItem("UserProfile_TissueList");
}
/*------------------------------Session_delete関数ここまで------------------------------*/
/*------------------------------JavaScriptここまで------------------------------*/
