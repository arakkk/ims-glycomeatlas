/*------------------------------SVGのクリックでクリックした組織画像の色を固定、LCListを取得し、糖鎖構造一覧を書き換える関数------------------------------*/
function click(obj) {
  // console.log("click object", obj);

  // クリックした時表示する
  const $dis = $("#wrapper_click_staining_info");
  const $organ_info = $("#click_organelles_events");

  const btn = $("#staining_button");
  //クリックしたSVG画像のIDを取得
  var svgID = Number(obj.id);
  var svgFile = obj.getAttribute("data-svgfile");
  // console.log("click object id filename ");
  // console.log(svgFile);

  // クリックオブジェクトに title タグがある場合
  if (obj.tagName === "g") {
    if (obj.children[0].tagName === "title") {
      const sList = "stainingList" + removeSpecialLetter(obj.children[0].textContent);
      const $cs = $("#compareStaining");
      const $sList = $("#" + sList);
      const $all = $(".staining_list");
      if (document.getElementById(sList)) btn.show();
      else btn.hide();
      $cs.prop("data-display-id", "#" + sList);

      // display staining list.
      $all.hide();
      $sList.show();
      $dis.html("Region: " + obj.children[0].textContent.split(":")[0] + "");
    }
  } else {
    $dis.html("");
  }

  // 図をクリックしている場合
  if (!isNaN(svgID)) {
    console.log("SVG ID: " + svgID);
    //SVGのIDから選択した組織と同じ組織の行を選び出し、TissueNameを取得する
    const tissue_name = document.getElementById("list_tr" + svgID).childNodes[3].innerHTML;
    const refCol = Number(document.getElementById("main").getAttribute("data-ref-col"));
    const $main_info = $("#main_info");

    //組織表のtbodyに属性data-selectを追加し、IDを付加する（現在選択している組織のID）
    document.getElementById("list_table").childNodes[2].setAttribute("data-select", svgID);
    query_function();
    $main_info.html("Tissue: " + tissue_name);
    // selecttissue(tissue_name);
    $organ_info.html("Tissue: " + tissue_name);
    // selecttissue(tissue_name + refCol);
  }
}

/*------------------------------click関数ここまで------------------------------*/
var picture = "Human_SVG.html";
$(document).ready(function () {
  $("#wholeimage").attr("src", picture);
});
