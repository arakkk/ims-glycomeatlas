function selecttissue(tissue_name) {
  console.log("Got tissue name " + tissue_name);
  wholetissuelist = JSON.parse(sessionStorage.getItem("WholeTissueList"));

  // H&Eimageの表示
  HEimage = document.getElementById("HEimage");
  HEimage.innerHTML = "";
  var viewer = OpenSeadragon({
    id: "HEimage",
    prefixUrl: "./source/images/openseadragon/",
    tileSources: "./source/" + wholetissuelist[tissue_name] + "/dzi/H&E_Annotated.dzi"
  });
  console.log("Display image", "./source/" + wholetissuelist[tissue_name] + "/dzi/H&E_Annotated.dzi");

  create_glycantable(tissue_name);
}

/*------------------------------組織画像、組織表、グラフなどの色を変更する関数------------------------------*/
function change_color(svg, table, color) {
  //もし、svgがundefinedだったら、エラーの原因になるのでこの後の処理を無視する
  if (svg === undefined || svg == null) {
  } else {
    switch (color) {
      case "pink":
        {
          svg.style.opacity = 0.7;
          svg.style.fill = "rgb(200,0,200)";
          svg.style.stroke = "rgb(130,0,130)";
          table.style.backgroundColor = "rgb(250,170,250)";
        }
        break;

      case "yellow":
        {
          svg.style.opacity = 0.7;
          svg.style.fill = "rgb(255,215,0)";
          svg.style.stroke = "rgb(200,115,0)";
          table.style.backgroundColor = "rgb(255,215,0)";
        }
        break;

      default:
        //"white":
        {
          svg.style.opacity = 0.6;
          svg.style.fill = "";
          svg.style.stroke = "";
          table.style.backgroundColor = "";
          //値を指定しない＝最初の状態に戻す
        }
        break;
    }
  }
}
/*------------------------------change_color関数ここまで------------------------------*/

/*------------------------------全てのTissueをLinearCodeで横断検索をかけて組織を色付けする------------------------------*/
function query_function() {
  //その生物種が持つ組織数を数える
  var tissue = document.getElementById("list_table").childNodes[2].childElementCount;
  for (var i = 0; i < 18; i++) {
    //TissueListを取得する
    var cnt = Get_TissueList();
    //組織表からi行目のIDを取得するために、まずは行のIDの一部を取得
    var selecting_tr_id = "list_tr";
    //クリックした行番号（から1引いたIDの持つ行）の要素を取得する
    var table = document.getElementById(selecting_tr_id + String(i));
    var tissue_name = table.childNodes[1].innerHTML; //組織名を取得
    //組織名、副組織名（なし）、その組織に存在する糖鎖数、現在のループ値を関数の引数にする
    coloring(tissue_name, "", cnt, i);
  }
  function coloring(tissue_name, subtissue_name, cnt, i) {
    //i番目のSVG画像のIDを取得（Profile Viewerの場合はnullを代入）
    var svg = document.getElementById(Number(i));
    //もし、選択した組織と同じ組織なら、その組織を黄色に色付けする（現在のループ値と、クリックした行のID値（をtbodyタグに埋め込んだ数値）が一緒なら選択された組織とみなす）
    //さらに、選択した糖鎖構造の背景色を桃色にする
    if (i == document.getElementById("list_table").childNodes[2].getAttribute("data-select")) {
      //組織表と組織画像の色を変更
      change_color(svg, table, "yellow");
    }
    //見つからなければ組織を白色にする
    else {
      change_color(svg, table, "white");
    }
  }
}
/*------------------------------query_functionここまで------------------------------*/

$(document).on("click", ".tissue_name", function () {
  console.log(this);
  var tableID = $(this).closest("tr").index(); //表のID(Number:0-15)を取得
  var table_subID = $(this).closest("td").index(); //表のID(Number:0-15)を取得
  // tableIDからtissue_nameを取得
  var tblTbody = document.getElementById("list_table");
  var tissue_name = tblTbody.rows[tableID + 1].cells[1].innerHTML + table_subID;
  // tableIDからtissue_nameを取得ここまで

  // tissue id diff from top (count from 1)
  const tissueIdDiff = 4;
  // row name
  const tname = tblTbody.rows[tableID + 1].cells[1].innerHTML;
  // dol name
  const colName = tblTbody.rows[tissueIdDiff].cells[table_subID].innerHTML;

  {
    const $sCls = $(".staining_list");
    const $wbtn = $("#staining_button");
    const $cs = $("#compareStaining");
    const id = "stainingList" + removeSpecialLetter(tname) + removeSpecialLetter(colName);
    const sId = "#" + id;
    const $sId = $(sId);
    const $click_info = $("#click_organelles_events");
    const $stain_info = $("#wrapper_click_staining_info");
    const $main_info = $("#main_info");

    // register data-display-id
    $cs.prop("data-display-id", sId);

    // show staining button
    if (document.getElementById(id)) $wbtn.show();
    else $wbtn.hide();

    $sCls.hide();
    $sId.show();

    // Click Event

    $main_info.html("Tissue: " + tname);
    $click_info.html("Tissue: " + tname);
    $stain_info.html("");
  }

  // console.log("Got table id "+tableID);

  //組織表に属性data-selectを追加し、IDを付加する（現在選択している組織のID）
  document.getElementById("list_table").childNodes[2].setAttribute("data-select", tableID);

  var get_Tissue_name_index = 1;
  // var tissue_name = this.childNodes[get_Tissue_name_index].innerHTML;
  // var tissue_name = this.innerHTML; //クリックした表のTissueNameを取得
  var subtissue_name = ""; //SubTissueNameも（Profile Viewerのときだけ）取得

  selecttissue(tissue_name);
});
/*------------------------------LCListを書き換えるon("click")イベントここまで------------------------------*/
