/* ---------------- create table --------------------*/
async function create_glycantable(tissue_name) {
  // 対応するinfo_glycanファイルの読み込み;
  const tissue = tissue_name.match("[A-Za-zs]+")[0];
  const col_number = tissue_name.match("\\d+")[0];
  let info_glycans_path = "./source";
  if (col_number == 3) {
    info_glycans_path += "/normal";
  }
  info_glycans_path += "/" + tissue.toLowerCase().replace(" ", "_") + "/info_glycans.json";
  let info_glycans;
  await $.get(info_glycans_path, function (data) {
    info_glycans = data;
    // console.log("info_glycans: ", info_glycans);
  });

  // 糖鎖構造一覧のtableの作成
  document.getElementById("selectimage").innerHTML = "";
  document.getElementById("seadragon-viewer").innerHTML = "";
  // table要素を生成
  var table = document.createElement("table");
  table.id = "list_rdf";
  table.className = "list_rdf";
  // tr部分のループ
  for (var i = 0; i < info_glycans["data"].length; i++) {
    // tr要素を生成
    var tr = document.createElement("tr");
    // th・td部分のループ
    for (var j = 0; j < 5; j++) {
      if (i === 0) {
        // th要素を生成
        var th = document.createElement("th");
        // th要素内にテキストを追加
        if (j === 0) {
          th.textContent = "Name";
          tr.appendChild(th);
        } else if (j === 1) {
          th.textContent = "GlyTouCanID";
          tr.appendChild(th);
        } else if (j === 2) {
          th.textContent = "m/z";
          tr.appendChild(th);
        } else if (j === 3) {
          th.textContent = "MainPositiveAdduct";
          // tr.appendChild(th);
        } else if (j === 4) {
          th.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
          tr.appendChild(th);
        }
      } else {
        if (wholetissuelist[tissue_name + "-data"].includes(info_glycans["data"][i]["m/z"].split(".")[0])) {
          // td要素を生成
          var td = document.createElement("td");
          // td要素内にテキストを追加
          if (j === 0) {
            td.innerHTML = info_glycans["data"][i]["Name"];
            tr.appendChild(td);
          } else if (j === 1) {
            GID = info_glycans["data"][i]["GlyTouCanID"];
            if (GID == "not found") {
              td.innerHTML = "not found";
            } else {
              td.innerHTML =
                '<a href="https://glycosmos.org/glycans/show?gtc_id=' +
                GID +
                '" style="font-size:14px;" target="_brank">' +
                GID +
                "</a>";
            }
            tr.appendChild(td);
          } else if (j === 2) {
            td.textContent = info_glycans["data"][i]["m/z"];
            tr.appendChild(td);
          } else if (j === 3) {
            td.textContent = info_glycans["data"][i]["MainPositiveAdduct"];
            // tr.appendChild(td);
          } else if (j === 4) {
            td.innerHTML = "";
            mz = info_glycans["data"][i]["m/z"].split(".")[0];
            td.innerHTML =
              '<a href="javascript:openimage(' +
              "'" +
              wholetissuelist[tissue_name] +
              "'" +
              "," +
              "'" +
              mz +
              "'" +
              ");" +
              '">' +
              '<svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" fill="currentColor" class="bi bi-card-image" viewBox="0 0 16 16">' +
              '<path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>' +
              '<path d="M1.5 2A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13zm13 1a.5.5 0 0 1 .5.5v6l-3.775-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12v.54A.505.505 0 0 1 1 12.5v-9a.5.5 0 0 1 .5-.5h13z"/>' +
              "</svg>" +
              "</a>";
            tr.appendChild(td);
          }
        }
      }
    }
    // tr要素をtable要素の子要素に追加
    if (tr.textContent != "") {
      table.appendChild(tr);
    }
  }
  query_function();
  // 生成したtable要素を追加する
  document.getElementById("selectimage").appendChild(table);
  // window.location.hash = "selectimage"
}

function openimage(tissue_name, mz) {
  stainid = document.getElementById("seadragon-viewer");
  stainid.innerHTML = "";
  var viewer = OpenSeadragon({
    id: "seadragon-viewer",
    prefixUrl: "./source/images/openseadragon/",
    tileSources: "./source/" + tissue_name + "/dzi/" + mz + ".dzi"
  });
}
