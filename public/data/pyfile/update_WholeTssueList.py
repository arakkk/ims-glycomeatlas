import glob
import json
import os


# images/のファイル一覧からwholetissuelistを生成
def main(path: str):
    j = open(path + "public/WholeTissueList.json", "r")
    data = json.load(j)
    for key in data:
        if "-data" not in key:
            # 初期化
            data[key + "-data"] = []
            # ファイル一覧を取得
            path_regexp = path + "public/source/" + data[key] + "/images/*"
            dir_list = glob.glob(path_regexp, recursive=True)
            for dir_path in dir_list:
                file_name = dir_path.split("/")[-1].split(".")[0]
                # ファイル情報をjsonに格納
                data[key + "-data"].append(file_name)
            # 重複の確認
            for i in range(len(data[key + "-data"])):
                for j in range(len(data[key + "-data"])):
                    if i != j and data[key + "-data"][i] == data[key + "-data"][j]:
                        print("duplicate in", key, "\t", data[key + "-data"][i])

    # 書き出し
    with open(path + "public/WholeTissueList.json", mode="wt", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=2)


# wholetissuelistからinfoglycansを生成
def create_info_glycans_from_wholetissuelist(path: str, outpath: str, tissue: str):
    j = open(path + "public/WholeTissueList.json", "r")
    data = json.load(j)
    mz_list = None
    for key in data:
        if key == tissue + "-data":
            mz_list = data[key]
            break
    assert mz_list is not None
    info_glycans = {"data": []}
    for _ in range(1000000):
        min_mz = 99999999999999999999
        for i in range(len(mz_list)):
            try:
                mz = int(mz_list[i])
                if mz < min_mz:
                    min_mz = mz
                    min_mz_index = i
            except:
                mz_list.pop(i)
                break
        if min_mz == 99999999999999999999:
            continue
        info_glycans["data"].append(
            {"Name": "", "Formula": "", "m/z": str(min_mz), "MainPositiveAdduct": ""}
        )
        mz_list.pop(min_mz_index)
        if len(mz_list) == 0:
            print("suucees")
            break
    print("number of data is", len(info_glycans["data"]))
    with open(path + outpath + "/info_glycans.json", mode="wt", encoding="utf-8") as f:
        json.dump(info_glycans, f, ensure_ascii=False, indent=2)


# data.txtのname一覧をinfo_glycansに順番に挿入していく
def update_info_glycans(path: str, datapath: str, tissue: str):
    j = open(path + datapath + "info_glycans.json", "r")
    info_glycans = json.load(j)
    f = open(path + datapath + "data.txt", "r")
    data = f.read()
    f.close()
    lines = data.split("\n")
    if lines[-1] == "":
        lines.pop(len(lines) - 1)
    assert len(info_glycans["data"]) == len(lines), (
        str(len(info_glycans["data"])) + "\t" + str(len(lines))
    )
    for i in range(len(info_glycans["data"])):
        info_glycans["data"][i]["Name"] = lines[i]
    with open(path + datapath + "info_glycans.json", mode="wt", encoding="utf-8") as f:
        json.dump(info_glycans, f, ensure_ascii=False, indent=2)


if __name__ == "__main__":
    path = "/Users/kouiti/localfile/glycomeatlas/ims-glycomeatlas/"
    # main(path)
    # create_info_glycans_from_wholetissuelist(path, "public/source/normal/kidney", "Kidney3")
    update_info_glycans(path, "public/source/normal/kidney/", "kidney")
