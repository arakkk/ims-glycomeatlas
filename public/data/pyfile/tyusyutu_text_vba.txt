Sub getAllText()
    Dim sld As Slide
    Dim sh As Shape
    Dim clm As Column
    Dim cl As Cell
    Dim art As SmartArtNode
    Dim grp As GroupShapes
    Dim result As String
    result = ""
    
    For Each sld In ActivePresentation.Slides
        For Each sh In sld.Shapes
            If sh.HasTextFrame Then
                result = result & sh.TextFrame.TextRange.Text
                Debug.Print sh.TextFrame.TextRange.Text
            ElseIf sh.HasTable Then
                For Each clm In sh.Table.Columns
                    For Each cl In clm.Cells
                        result = result & cl.Shape.TextFrame.TextRange.Text
                        Debug.Print cl.Shape.TextFrame.TextRange.Text
                    Next
                Next
            ElseIf sh.HasChart Then
                If sh.Chart.HasTitle Then
                    result = result & sh.Chart.Title
                    Debug.Print sh.Chart.Title
                End If
            ElseIf sh.HasSmartArt Then
                For Each art In sh.SmartArt.Nodes
                    result = result & art.TextFrame2.TextRange.Text
                    Debug.Print art.TextFrame2.TextRange.Text
                Next
            ElseIf sh.Type = msoGroup Then
                For Each gsh In sh.GroupItems
                    If gsh.HasTextFrame Then
                        result = result & gsh.TextFrame.TextRange.Text
                        Debug.Print gsh.TextFrame.TextRange.Text
                    End If
                Next
            End If
        Next
        result = result & vbCrLf
    Next
    Dim datFile As String
    datFile = ActivePresentation.Path & "/data.txt"
    Debug.Print datFile
    Open datFile For Output As #1
    Print #1, result
    Close #1
    MsgBox "????"
End Sub

