import urllib.parse
import json
import re
import sys
import traceback

import requests

# 多分info_glycansに既に登録されているコンポジションからglytoucanidを取得するpythonプログラム

def get_unregistered_compositions(path, cor_dict, tissue) -> str:
    j = open(path + "public/source/" + tissue + "/info_glycans.json", "r")
    info_glycans = json.load(j)
    with open(path + "public/source/" + tissue + "/wurcs.txt", "w") as f:
        f.write("")
    for i in range(len(info_glycans["data"])):
        name = info_glycans["data"][i]["Name"]

        name = re.sub(r"\s", "", name)
        # escape Na
        name = re.sub(r"\+\d+Na", "", name)
        re_text = ""
        composition = {}

        for cor_regexp in cor_dict:
            re_composition = re.search(cor_regexp, name)
            if re_composition is None:
                continue
            comps = re_composition.group()
            count = re.sub(cor_regexp, r"\g<count>", comps)
            if count == "":
                count = "0"
            # for test
            name = re.sub(cor_regexp, "", name)
            re_text += count + " " + cor_dict[cor_regexp] + ", "
            composition[cor_dict[cor_regexp]] = str(count)

        # for test
        if name != "":
            print("error", info_glycans["data"][i]["m/z"], name)
            sys.exit()

        params = json.dumps([composition])
        headers = {"content-type": "application/json"}
        res = requests.post(
            "https://api.glycosmos.org/glycancompositionconverter/0.0.1/c2w",
            params,
            headers=headers,
        )
        try:
            content = res.json()["data"][0]
            wurcs = content["wurcs"]
            encode_wurcs = urllib.parse.quote(wurcs)
            res2 = requests.get("https://api.glycosmos.org/sparqlist/wurcs2gtcids?wurcs="+encode_wurcs)
            try:
                glytoucan_id = res2.json()[0]["id"]
                print("------------------")
                print(info_glycans["data"][i]["Name"])
                print(composition)
                print(glytoucan_id)
                info_glycans["data"][i]["GlyTouCanID"] = glytoucan_id
            except:
                with open(path + "public/source/" + tissue + "/wurcs.txt", "a") as f:
                    f.write(content["wurcs"] + "\n")
        except:
            print("Error in", info_glycans["data"][i]["Name"])
            traceback.print_exc()
            sys.exit()
    # 書き出し
    with open(
        path + "public/source/" + tissue + "/info_glycans.json",
        mode="wt",
        encoding="utf-8",
    ) as f:
        json.dump(info_glycans, f, ensure_ascii=False, indent=2)

    return "1"


if __name__ == "__main__":
    path = "/Users/kouiti/localfile/glycomeatlas/ims-glycomeatlas/"
    cor_dict = {
        r"HexNAc(?P<count>(\d+))": "hexnac",
        r"dHex(?P<count>(\d+))": "dhex",
        r"Hex(?P<count>(\d+))": "hex",
        r"NeuAc(?P<count>(\d+))": "neu5ac",
        r"NeuGc(?P<count>(\d+))": "neu5gc",
        r"Fuc(?P<count>(\d+))": "dhex",
        # "": "P",
        r"\+(?P<count>((\d+)?))SO4": "S",
        # "": "Ac",
    }
    get_unregistered_compositions(path, cor_dict, "normal/kidney")
