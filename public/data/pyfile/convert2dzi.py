import os
import sys
from pathlib import Path
# sys.path.append("/Users/kouiti/localfile/glycomeatlas/ims-glycomeatlas/public/")
# sys.path.append("../../")

import dzt_source

""" 
    This dzi.py is used for coverting photo images to zooming file 
    with extension .dzi 
"""

def main(path: str, file_type: str):
    try:
        os.mkdir(path + "dzi")
    except:
        pass
    # Specify your source image
    path_regexp = path + "images/*." + file_type
    images = [str(p) for p in Path(".").glob(path_regexp)]

    # Create Deep Zoom Image creator with weird parametersa
    creator = dzt_source.ImageCreator(
        tile_size=256,
        tile_overlap=4,
        tile_format=file_type,
        image_quality=1.0,
        resize_filter="bicubic"
    )

    # Create Deep Zoom image pyramid from source
    for image, img_path in zip(images, images):
        img_name = img_path.split("/")[-1].split(".")[0]
        print("convert", img_name)
        url = path + "dzi/" + img_name + ".dzi"
        creator.create(image, url)

# tifはjpgに変換して
def tif_jpg2png(path):
    count = 0
    path_regexp = path + "images/*.tif"
    images = [str(p) for p in Path(".").glob(path_regexp)]
    count += len(images)
    for img_path in images:
        os.rename(img_path, img_path+".png")
    path_regexp = path + "images/*.jpg"
    images = [str(p) for p in Path(".").glob(path_regexp)]
    for img_path in images:
        os.rename(img_path, img_path+".png")
    count += len(images)
    print("converted", count, "files")


# document.querySelector("#list_rdf").children.length-1


# .tif形式の画像はpngフォルダに入れておく
if __name__ == "__main__":
    # path = "public/source/normal/pancreas/"
    path = "public/source/normal/kidney/"

    tif_jpg2png(path)
    main(path, "tiff")
    main(path, "png")
