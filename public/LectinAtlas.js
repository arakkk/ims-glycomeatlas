/*------------------------------ここからJavaScript------------------------------*/
// path id for Thymus whole: path4577
//var local = ""
var local = ".";
var url = location.href;
if (url.includes("#")) {
  window.location.href = url.split("#")[0];
}
/*------------------------------グローバル変数の宣言------------------------------*/
//IDname=resultのdocumentオブジェクトをresult変数に
var r = document.getElementById("result");

/*------------------------------グローバル変数の宣言ここまで------------------------------*/

/*------------------------------get URL parameters------------------------------*/
function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
    vars[key] = decodeURI(value);
  });

  return vars;
}
/*------------------------------getUrlVarsここまで------------------------------*/

/*------------------------------非同期通信関数の設定 ------------------------------*/

/*------------------------------初期設定（起動時の処理）------------------------------*/
/*ここの処理で、TissueListの読み込み、LCListの読み込み、SVGファイルの読み込みの計3種類の処理を行っている*/
/*ここでのループは生物種の数や組織の数によって条件が変動するようになっているため、今後増えたとしても書き換える必要はない*/
//TissueListの読み込みと、sessionStorageへの保存
$.get("./WholeTissueList.json", function (data) {
  sessionStorage.setItem("WholeTissueList", JSON.stringify(data)); //WholeTissueListを保存
  // console.log("WholeTissueList data; "+JSON.stringify(data));
});

$.get("./TissueList.json", function (data) {
  // console.log("TissueList data; "+JSON.stringify(data));
  sessionStorage.setItem("TissueList", JSON.stringify(data)); //TissueListを保存

  // default: Mouse
  selectSpecies = "Human";
  selectTissue = getUrlVars()["tissue"];

  //TissueListの読み込みと、sessionStorageへの保存
  var TissueList = Get_TissueList(); //TissueListを取得
  const refCol = Number(location.pathname.split(".")[0].slice(-1)) + 2;
  const sub_id = typeof refCol === NaN ? "" : refCol;
  if (sub_id !== "")
    $.get("./html/Human_SVG.html", function (data) {
      //sessionStorage.setItem(filename, data);
      const main = document.getElementById("main");
      main.innerHTML = data;
      main.setAttribute("data-ref-col", sub_id);
    });
});

$("#tabs").tabs({
  create: function (event, ui) {
    var dps1 = [];
    var dps2 = [];
    var dps3 = [];
    var dps4 = [];
    var dps5 = [];
    var options1 = {
      animationEnabled: true,
      title: {
        text: "Lectin Array Data per Sample"
      },
      legend: {
        fontSize: 8,
        //dockInsidePlotArea: true,
        horizontalAlign: "left",
        verticalAlign: "top"
      },
      axisY: {
        title: "Normalized intensity",
        suffix: "",
        includeZero: true
      },
      axisX: {
        title: "Lectins",
        labelAngle: 270,
        interval: 1,
        labelAutoFit: true
      },
      data: [
        {
          type: "column",
          showInLegend: "true",
          dataPoints: dps1
        }
      ]
    };
    var options2 = {
      animationEnabled: true,
      title: {
        text: "Average Lectin Array Data"
      },
      legend: {
        //fontSize: 8,
        //dockInsidePlotArea: true,
        horizontalAlign: "center",
        verticalAlign: "top"
      },
      axisY: {
        title: "Normalized intensity",
        suffix: "",
        includeZero: true
      },
      axisX: {
        title: "Lectins",
        labelAngle: 270,
        interval: 1,
        labelAutoFit: true
      },
      data: [
        {
          type: "column",
          showInLegend: "true",
          legendText: "Average Lectin Values",
          dataPoints: dps2
        },
        {
          type: "error",
          whiskerLength: 15,
          name: "STD",
          showInLegend: "true",
          dataPoints: dps3
        }
      ]
    };
    //Render Charts after tabs have been created.
    $("#chartContainer1").CanvasJSChart(options1);
    $("#chartContainer2").CanvasJSChart(options2);
  }
});

/*------------------------------初期設定（起動時の処理）ここまで）------------------------------*/

function swipe() {
  console.log("Entered swipe");
  var largeImage = document.getElementById("sampleimage");
  largeImage.style.display = "block";
  largeImage.style.width = 200 + "px";
  largeImage.style.height = 200 + "px";
  var url = largeImage.getAttribute("src");
  window.open(url, "Image", "width=largeImage.stylewidth,height=largeImage.style.height,resizable=1");
}

function Create_Sample_View(samplefile) {
  console.log("Entered create sample view");
  var largeImage = document.createElement("img"); //document.getElementById('sampleimage');
  largeImage.style.display = "block";
  largeImage.style.width = 100 + "%";
  largeImage.style.height = 100 + "%";
  var url = samplefile;
  window.open(url, "SampleView", "width=largeImage.stylewidth,height=largeImage.style.height,resizable=1");
}

/* ---------------- Display sample images in upper right pane --------------------*/
function Create_Sample_View_Old(samplefile) {
  console.log("Entered Create Sample View with samplefile " + samplefile);
  //r.innerHTML = "";
  var make_table = "<tr id='Sample_tr'>";
  make_table += "</tr>";
  var viewing = document.getElementById("viewing");
  var dtable = document.getElementById("Detailed_table");
  //viewing.innerHTML = make_table;
  //viewing.innertHTML = "";
  $("#sampleimage").remove();

  var embed = document.createElement("img");
  embed.setAttribute("id", "sampleimage");
  embed.setAttribute("src", samplefile);
  embed.setAttribute("type", "image/svg+xml");
  embed.setAttribute("width", "400");
  embed.setAttribute("onClick", "swipe();");

  viewing.appendChild(embed);
}
/* ---------------- End of Display sample images in upper right pane  --------------------*/

/*------------------------------JSONをパースしたlectin_array_dataを返す関数------------------------------*/
function Get_LectinArrayData() {
  return JSON.parse(sessionStorage.getItem("LectinArrayData"));
}
/*------------------------------Get_TissueList関数ここまで------------------------------*/
/*------------------------------JSONをパースしたTissueListを返す関数------------------------------*/
function Get_TissueList() {
  return JSON.parse(sessionStorage.getItem("TissueList"));
}
/*------------------------------Get_TissueList関数ここまで------------------------------*/
/*------------------------------JSONをパースしたWholeTissueListを返す関数------------------------------*/
function Get_WholeTissueList() {
  return JSON.parse(sessionStorage.getItem("WholeTissueList"));
}
/*------------------------------Get_TissueList関数ここまで------------------------------*/

/*------------------------------表をクリックすることで組織別によって取得するWholeTissueを変更するイベント------------------------------*/
$(document).on("contextmenu", ".list_td", function (e) {
  // download link
});

/* ---------- Exclude Spedial Charachter '(' ')' '/' for jQuery Expression ---------- */
function removeSpecialLetter(str) {
  return str.split(/\s+/).join("").split(/\(/).join("").split(/\)/).join("").split(/\//).join("");
}

function display_whole(tissue_name, mz) {
  // console.log("Entered whole with "+selectTissue);
  $("#wholeimage").remove();
  $("#sampleimage").remove();
  $("#zoom-in").remove();
  $("#zoom-out").remove();
  $("#reset").remove();
  $("#brline").remove();

  query_function();
  var tissuefile = "data/images/" + tissue_name + "/svg/" + mz + ".svg";

  //var $wholeobjstr = "<object id=\"wholeimage\" data='" + tissuefile + "' type=\"image/svg+xml\" width='400'></object>";

  var embed = document.createElement("object");
  embed.setAttribute("id", "wholeimage");
  embed.setAttribute("data", tissuefile);
  embed.setAttribute("type", "image/svg+xml");
  embed.setAttribute("alt", "whole image");
  embed.setAttribute("width", 400);
  embed.setAttribute("height", 500);

  var boundingRect = embed.getBoundingClientRect();
  /*
    var newWidth = boundingRect.width + 75;
    embed.setAttribute('width',newWidth.toString());
    embed.setAttribute('viewBox', "0 0 "+newWidth.toString()+" "+boundingRect.height.toString());
     */

  // zooming function button in the right position of index.html
  var ctrlbutton1 = document.createElement("button");
  ctrlbutton1.setAttribute("id", "zoom-in");
  ctrlbutton1.innerHTML = "Zoom In";
  var ctrlbutton2 = document.createElement("button");
  ctrlbutton2.setAttribute("id", "zoom-out");
  ctrlbutton2.innerHTML = "Zoom Out";
  var ctrlbutton3 = document.createElement("button");
  ctrlbutton3.setAttribute("id", "reset");
  ctrlbutton3.innerHTML = "Reset";

  const $btns = $("#zoomConfig");
  const $table = $("#Detailed_table");
  $table.prepend(embed);
  var brline = document.createElement("br");
  brline.setAttribute("id", "brline");
  $table.append(brline);
  $btns.append(ctrlbutton1).append(ctrlbutton2).append(ctrlbutton3);

  var lastEventListener = function () {
    var panZoom = svgPanZoom(embed, {
      controlIconsEnabled: false,
      minZoom: 1,
      mouseWheelZoomEnabled: false
    });
    document.getElementById("zoom-in").addEventListener("click", function (ev) {
      ev.preventDefault();
      panZoom.zoomIn();
    });
    document.getElementById("zoom-out").addEventListener("click", function (ev) {
      ev.preventDefault();
      panZoom.zoomOut();
    });
    document.getElementById("reset").addEventListener("click", function (ev) {
      ev.preventDefault();
      panZoom.resetZoom();
    });
  };
  embed.addEventListener("load", lastEventListener);

  // console.log("Displayed whole " + tissuefile);
}

/*------------------------------エラーメッセージをモーダルウィンドウで表示する処理------------------------------*/
function modal() {
  Open_Profile_Viewer ? $(".PV_overlay").fadeIn(100) : $(".overlay").fadeIn(100);
  $("#not_have_number").fadeIn(100); //エラーメッセージを表示

  //スクロールをできなくする
  document.body.style.overflow = "hidden";

  //✕ボタン、closeボタンが押されたらウィンドウを非表示に戻し、スクロールできるようにする
  $(".close , .cross").on("click", function () {
    $(".overlay, #not_have_number").fadeOut(100);
    document.body.style.overflow = "visible";
  });

  locateCenter(); // => モーダルウィンドウを中央配置するための初期値を設定する
  $(window).resize(locateCenter);
}
/*------------------------------modal関数ここまで------------------------------*/

/*------------------------------保存したユーザープロファイルを削除する関数------------------------------*/
function Session_delete() {
  sessionStorage.removeItem("UserProfile_TissueList");
  sessionStorage.removeItem("WholeTissueList");
  sessionStorage.removeItem("LectinArrayData");
}

/*------------------------------JavaScriptここまで------------------------------*/
