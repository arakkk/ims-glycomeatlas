# IMS-GlycomeAtlas
## Build手順
```
git clone
cd
npm install express
node app.js
```
## リンク


## データの追加方法

### dzi画像の追加
- directory
    - ./source/{tissue_type (normal)}/{tissue_name (pancreas)}/dzi
- 作成方法
    - png or tiff画像を./source/{tissue_type}/{tissue_name}/images/下に用意
    - ./source/normal/pancreas/tiff/*.tiff
    - `pip -r install data/pyfile/requirements.txt`を実行
    - data/pyfile/convert2dzi.pyを変更/実行
**H&E_Annotated 画像の追加も**
    - ./source/images/normal/pancreas/dzi/H&E_Annotated.dzi

### WholeTissueList.jsonの編集
- example
```
{
    "Pancreas3":"normal/pancreas",	// dzi image directory
    "Pancreas3-data":[			// list of image's m/s and H&E
	"1866.tiff",
	"2067.tiff",
	"H&E_Annotated.tif"
    ]
}
```
`python data/pyfile/convert2dzi.py`


### glycans.jsonの作成
- directory
    - ./source/{tissue_type (normal)}/glycans.json
- example
```
{
// 3 is column number
"Pancreas3": [
    {
	"Name": "Hex2HexNAc2",
	"Formula": " (C6H10O5)2(C8H13NO5)2(H2O)",
	"m/z": "771.2822",
	"MainPositiveAdduct": "[M+Na]+",
	"undefined": "Unknown Prostate"
    }
}
```

# pyfiles
data/pyfile/seachGlyTouCanId.py
*多分info_glycansに既に登録されているコンポジションからglytoucanidを取得するpythonプログラム*
data/pyfile/update_WhileTissueId.py
**
