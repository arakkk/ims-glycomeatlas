const http = require("http");
const fs = require("fs");
const express = require("express");

const hostname = "127.0.0.1";
const port = process.env.PORT || 3300;
const path = process.env.PROJECTNAME || "";

const server = http.createServer((req, res) => {
  const url = "public" + (req.url.endsWith("/") ? req.url + "index.html" : req.url.split("?")[0]);
  console.log(url);
  if (fs.existsSync(url)) {
    fs.readFile(url, (err, data) => {
      if (!err) {
        res.writeHead(200, { "Content-Type": getType(url) });
        res.write(data);
        res.end();
      }
    });
  }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

function getType(_url) {
  var types = {
    ".html": "text/html",
    ".css": "text/css",
    ".js": "text/javascript",
    ".png": "image/png",
    ".gif": "image/gif",
    ".svg": "svg+xml",
    ".json": "application/json"
  };
  for (var key in types) {
    if (_url.endsWith(key)) {
      return types[key];
    }
  }
  return "text/plain";
}
